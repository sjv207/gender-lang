from time import sleep
from datetime import datetime
from otree.api import *
from .pages import *
import logging

# Scores: [rows][columns], where 'rows' are rounds, and 'columns' are the players, 1-4
scores = [
    [1,1,1,1],
    [4,5,6,7],
    [6,7,7,7],
    [8,7,6,5],
    [2,3,4,5],
    [7,6,5,4],
]

# Best guesses for their performance in Stage 2
stage2_perfs = [4,1,1,2]
# True - worst score, True - best score, True - joint best score, False - was joing best, not second
stage2_perf_bonuses = [True, True, True, False]

# Modes for stage 3, 1=Piece rate, 2=Competition
s3_modes = [1,1,2,2]

class PlayerBot(Bot):
    def play_round(self):

        # Vary the scores with the players and rounds
        score = scores[self.round_number-1][self.player.id_in_group-1]
        # print(f"~~~~~ Round: {self.round_number}, player: {self.player.id_in_group}, score: {score}")
        data = dict(correct=score, total=score)
        self.player.live_update(data)
        print(f"0. Player: {self.player.id},{self.player.id_in_group}. Data {data}, {self.player.num_correct}, {self.player.num_total}. round: {self.round_number}")
        # yield live_update(self.player, data)

    
        if self.round_number == 1:
            yield TrialInstructions
            yield Submission(Trial, dict(clickstream="clicks"), check_html=False)
            yield Start

        if self.round_number == 2:
            yield Submission(Matrix, dict(clickstream="clicks"), check_html=False)
            yield Stage1End, dict(stage1_perf=1)

        if self.round_number == 3:
            live_update(self.player, data)
            print(f"1. Player: {self.player.id},{self.player.id_in_group}. Data {data}, {self.player.num_correct}, {self.player.num_total}. round: {self.round_number}")
            yield Stage2Instructions
            yield Submission(Matrix, dict(clickstream="clicks"), check_html=False)
            yield Stage2End, dict(stage2_perf=stage2_perfs[self.player.id_in_group-1])
            print(f"2. PlayerID: {self.player.id_in_group}, pref: {stage2_perfs[self.player.id_in_group-1]}, p.pref {self.player.stage2_perf_bonus}")
            # Players 1 and 4 should have got paid this bonus
            expect(self.player.stage2_perf_bonus, stage2_perf_bonuses[self.player.id_in_group-1])

        elif self.round_number == 4:
            yield Stage3Instructions
            mode = s3_modes[self.player.id_in_group-1]
            yield Stage3Decision, dict(stage3_mode=mode)
            yield Submission(Matrix, dict(clickstream="clicks"), check_html=False)

        elif self.round_number == 5:
            yield Stage4Instructions
            yield Submission(Matrix, dict(clickstream="clicks"), check_html=False)

        elif self.round_number == 6:
            if self.player.id_in_group == 3 or self.player.id_in_group == 4:
                if self.session.config['genderise'] == 'he':
                    yield Stage5Instructionsm, dict(stage5_election=True)
                elif self.session.config['genderise'] == 'she':
                    yield Stage5Instructionsf, dict(stage5_election=True)
                elif self.session.config['genderise'] == 'they':
                    yield Stage5Instructionst, dict(stage5_election=True)
            else:
                if self.session.config['genderise'] == 'he':
                    yield Stage5Instructionsm, dict(stage5_election=False)
                elif self.session.config['genderise'] == 'she':
                    yield Stage5Instructionsf, dict(stage5_election=False)
                elif self.session.config['genderise'] == 'they':
                    yield Stage5Instructionst, dict(stage5_election=False)
            # Want to make sure that 2 players end up with the same vote, so one gets randomly chosen
            if self.player.id_in_group == 1:
                yield Submission(Stage5Voting, dict(s4_ordering="[1,2,3]", s4_rank1=1, s4_rank2=2, s4_rank3=3), check_html=False)
            if self.player.id_in_group == 2:
                yield Submission(Stage5Voting, dict(s4_ordering="[1,3,2]", s4_rank1=1, s4_rank2=3, s4_rank3=2), check_html=False)
            if self.player.id_in_group == 3:
                yield Submission(Stage5Voting, dict(s4_ordering="[1,2,3]", s4_rank1=1, s4_rank2=2, s4_rank3=3), check_html=False)
            if self.player.id_in_group == 4:
                yield Submission(Stage5Voting, dict(s4_ordering="[1,2,3]", s4_rank1=1, s4_rank2=2, s4_rank3=3), check_html=False)

            if self.player.stage5_election or self.player.s5_group_leader:
                print(f"Player {self.player.id_in_group} playing matrix")
                yield Submission(Matrix, dict(clickstream="clicks"), check_html=False)
            else:
                print(f"Player {self.player.id_in_group} sitting it out")


        # Might want to comment this out so we can see the results page
        logger.info(f"--- pre-results: {self.round_number} for player: {self.player.id_in_group}")
        if self.round_number == Constants.num_rounds and self.session.config['show_debug_results'] and self.player.is_playing():
            print(f"*** --- done, going to results: {self.round_number} for player: {self.player.id_in_group}")
            yield Results

def  live_update(player:Player, data):
    player.num_correct = data['correct']
    player.num_total = data['total']
