��    P      �  k         �  �   �  �   �  �   �  �   2	  �   �	  �   �
  �  �    S  /  U  �   �  7     6   @  5   w  �   �  2   z  [   �  �   	  �  �  ~  |  �   �  `  �  2  �  �    �  �  _   �  4     5   T  6   �     �     �     �  _      X   b      �      �      �      �   �   �      �!  P   �!  Q   �!  R   E"  [   �"  \   �"  ]   Q#  #   �#  )   �#  +   �#  +   )$  A   U$  B   �$  C   �$  L   %  3   k%     �%     �%     �%     �%     �%  '   �%  (   �%  n   &  o   |&  p   �&  K   ]'     �'     �'     �'     �'     (     (  	   $(  )   .(  '   X(  4   �(  &   �(     �(     �(  !   �(  "  )  �   8*  �   "+  �   ,  �   �,  �   r-  �   !.  �  �.    �0  O  �1  �   3  T   �3  M   �3  C   74  �   {4  ,   ?5  h   l5  �   �5  �  �6  �  �:  �   �<  J  6=  K  �>  &  �?    �A  �   	D  E   �D  ^   �D  H   /E     xE     �E  )   �E  v   �E  [   OF     �F     �F     �F     �F  �   �F  .   �G  _   �G  �   .H  j   �H  d   I  t   �I  f   �I  ;   ^J  <   �J  D   �J  N   K  >   kK  R   �K  G   �K  ?   EL  K   �L  	   �L     �L     �L     �L     M  +   M  ,   4M  �   aM  �   �M  �   mN  B   �N     6O     KO     `O     uO     �O  
   �O     �O  )   �O  /   �O  ,   P  (   =P     fP  !   iP  -   �P        H         ;       N   3          K   &   +       ?   $   2      6   !   /      >   '   A           J   9   <                             
   .   *   %             )   L                                        	              7   (   =              4             B   P      :   I   O       0   D   8   E       @   1                 5   M          #                 C   "   ,   F   G      -        

<p>For the bonus payment, the conversion rate for ECU is %(ecus)s ECU = %(conversion_rate)s. Therefore, your total
    compensation (for participating in this study plus your bonus) is %(payoff_plus_participation_fee)s</p>
 
                Please tell us, in general, how willing or unwilling you are to take risks. Where 0
                means "completely unwilling to take risks" and 10 "very willing to take risks".
                 
            <td></td>
            <td>All female</td>
            <td></td>
            <td>Equal balance</td>
            <td></td>
            <td>All male</td>
             
            <td></td>
            <td>Not very much at all</td>
            <td>Not much</td>
            <td>Somewhat</td>
            <td>Quite a lot</td>
            <td>Very Much</td>
             
            <td>Strongly disagree</td>
            <td>Somewhat disagree</td>
            <td>Neither agree nor disagree</td>
            <td>Somewhat agree</td>
            <td>Strongly agree</td>
             
    <p><strong> Thank you for participating in this study! </strong></p>
    <p style="color: red;">Please click this link to take you to the <strong>FEEL payment app: <a href="%(fwd_url)s">FEELE Payment</a></strong></p>

     
    <strong> Thank you for participating in this study! </strong></p>

    <p><b  style="color: red;">We need your email address through which to pay you</b>. This information will not be associated with the decisions
        that you have made today, or linked with the experimental data that has been recorded. It will not be used for any
        purpose other than issuing payment for this experiment.</p>
     
    <strong> Thank you for participating in this study! </strong></p>

    <p>You have now completed the study, please click
        <a href="%(prolific_url)s">HERE</a>
        to return this study to Prolific, which will ensure your payment.
    </p>
     
<p></p>
<p>We were unable to group you with other people in the time allocated, so you could not proceed with the study. We
    would like to reimburse you for your time.</p>
<p><strong>Please click this link to take you to the FEEL payment app: <a href="%(fwd_url)s">FEELE Payment</a></strong>

</p>

 
<p><strong>Please click this link to take you to the FEEL payment app: <a href="%(fwd_url)s">FEELE Payment</a></strong></p>
    
 
<p>Are you currently a student at a university?  </p>
 
<p>Are you currently a student at a university? </p>
 
<p>Are you currently a student at a university?</p>
 
<p>For the following statements, please indicate on a scale from 1 to 5 to what extent you agree with each statement
    (where 1 indicates "completely disagree" and 5 indicates "completely agree").</p>
 
<p>In which country do you currently reside?</p>
 
<p>Please return this study to Prolific and we will pay you through the bonus feature</p>
 
<p>Thank you for your time today, and for taking part in our experiment.</p>
<p>We will make payment to the email address <strong>"%(email)s"</strong> in the next few days</p>
 
<p>Thank you for your time today, and for taking part in our experiment.</p>
<p>You took part in 5 stages, and as detailed at the beginning of the experiment we have randomly selected one of those
    stages for you final payment bonus.</p>



<table class="table table-sm table-striped">
    <tr>
        <td>Bonus Stage</td>
        <td>%(bonus_stage)s</td>
    </tr>
    <tr>
        <td>Bonus Payment</td>
        <td>%(bonus_payment)s</td>
    </tr>
    <tr>
        <td>Stage 1 Question bonus</td>
        <td>Please note this bonus will be<br>calculated and paid once<br>the study is complete</td>
    </tr>
    <tr>
        <td>Stage 2 Question bonus</td>
        <td>%(stage2_q_bonus)s</td>
    </tr>
    <tr>
        <td>Instruction variation bonus</td>
        <td>%(gender_bonus)s ECU</td>
    </tr>
    <tr>
        <td>
            <h3>Total Payoff</h3>
        </td>
        <td>
            <h3>%(total)s</h3>
        </td>
    </tr>
</table>
 
<p>Unfortunately a member of your group failed to interact with the page "%(pagename)s" in the time allotted,
    and is therefore considered to have dropped out of the study.</p>
<p>This is incredibly frustrating for us and you. We will pay you %(compensation)s as compensate for your time</p>

<p>Thank you for your time today, we are sorry you could not complete the study.</p>
 
<p>Unfortunately you did not consent to the terms and conditions. Therefore you cannot take part in our experiment and will not be paid</p>

 
<p>Unfortunately you failed to interact with the page: <strong>"%(pagename)s"</strong> in the time allotted,
    you therefore dropped out of the study and will not be paid.</p>
<p>This is incredibly frustrating for us. We have had to compensate the other members of your group for their time, and
    we have no experimental data to show for it</p>

 
<p>We need <b>your email address through which to pay you</b>. This information will not be associated with the decisions
    that you have made today, or linked with the experimental data that has been recorded. It will not be used for any
    purpose other than issuing payment for this experiment.</p>
 
<p>We were unable to group you with other people in the time allocated, so you could not proceed with the study. We
    would like to reimburse you for your time. We have to do this using the bonus function, so we need you to return
    your survey on Prolific, so that we can make this payment. Please send us a message through the Prolific system to
    let us know that you have returned the survey as soon as possible. Thank you for your time.</p>
 
<p>We were unable to group you with other people in the time allocated, so you could not proceed with the study. We
    would like to reimburse you for your time.</p>

<p>We need <b>your email address through which to pay you</b>. This information will not be associated with the decisions
    that you have made today, or linked with the experimental data that has been recorded. It will not be used for any
    purpose other than issuing payment for this experiment.</p>
 ... and finally, please let us have some feedback about your experience during this experiment. At work, is your direct line manager female or male? At work, is your direct line manager female or male?  At work, is your direct line manager female or male?   Completely<br>agree Completely<br>disagree Confirm email address Did you find one or more parts of the study strange? Or did you wonder about one or more parts? Do you think some of the parts of the study are related? If so, what was the connection? Dropout Dropout Victim Email address Female Has anything you did in one part influenced your behaviour or performance in another part? If so, how exactly was your behaviour (your performance) affected? I felt included in my group. I felt less justified than other participants to be the group leader in Stage 5. I felt less justified than other participants to be the group leader in Stage 5.  I felt less justified than other participants to be the group leader in Stage 5.   I felt less justified than other participants to participate in the competition in Stage 3. I felt less justified than other participants to participate in the competition in Stage 3.  I felt less justified than other participants to participate in the competition in Stage 3.   I felt like I belonged to my group. I felt like an equal member of the group. I made my decisions in this study carefully I made the decisions in this study randomly I thought it was inappropriate to be the group leader in Stage 5. I thought it was inappropriate to be the group leader in Stage 5.  I thought it was inappropriate to be the group leader in Stage 5.   I thought it was inappropriate to participate in the competition in Stage 3. I understood what my decisions meant for my payment Male NA No Non-Consent Other Please check the option on the far left Please check the option on the far right Please consider the people in your life who are role models to you. What share of them are female versus male? Please consider the people in your life who are role models to you. What share of them are female versus male?  Please consider the people in your life who are role models to you. What share of them are female versus male?   Please indicate the extent to which you agree with the following statements Questionnaire - page 1 Questionnaire - page 2 Questionnaire - page 3 Questionnaire - page 4 Questionnaire - page 5 Results Thank You To what extent do you support this party? What do you think this study was about? What is your highest education/degree you completed? Which party do you typically vote for? Yes You were ungrouped Your email addresses do not match Project-Id-Version: 
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2022-11-23 08:11+0000
Last-Translator: 
Language-Team: 
Language: de
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 3.0.1
 
<p>Für die Bonuszahlung lautet der Umrechnungskurs für ECU %(ecus)s ECU = %(conversion_rate)s. Daher beträgt Ihre Bezahlung 
 (für die Teilnahme an dieser Studie plus Ihrem Bonus) insgesamt %(payoff_plus_participation_fee)s</p>
 
                Bitte teilen Sie uns im Allgemeinen mit, wie willig oder unwillig Sie sind, Risiken einzugehen. 0
                bedeutet "ich vermeide jedes Risiko" und 10 "ich bin sehr bereit, Risiken einzugehen".
                 
            <td></td>
            <td>Alle weiblich</td>
            <td></td>
            <td>Ausgeglichen</td>
            <td></td>
            <td>Alle männlich</td>
  
           <td></td>
           <td>Überhaupt nicht</td>
           <td>Nicht viel</td>
           <td>Etwas</td>
           <td>Ziemlich viel</td>
            <td>Sehr viel</td>
  
 <td>Stimme überhaupt nicht zu</td>
 <td>Stimme eher nicht zu</td>
 <td>Stimme weder zu noch nicht zu</td>
 <td>Stimme teilweise zu</td>
 <td>Stimme voll und ganz zu</td>
  
<p><strong> Vielen Dank für Ihre Teilnahme an dieser Studie! </strong></p>
<p style="color: red;">Bitte klicken Sie auf diesen Link, um zum <strong>FEELE-Zahlungs-App: <a href="%(fwd_url)s">FEELE-Zahlung</a></stark></p> 
<strong> Vielen Dank für Ihre Teilnahme an dieser Studie! </strong></p>

<p><b style="color: red;">Wir benötigen Ihre E-Mail-Adresse, um Sie zu bezahlen</b>. Diese Information wird nicht mit Ihren Entscheidungen 
 die Sie heute gemacht haben, oder mit den experimentellen Daten verknüpft, die aufgezeichnet wurden. Die E-Mail-Adresse wird ausschließlich für die 
Bezahlung dieser Studie verwendet.</p> 
<strong> Vielen Dank für Ihre Teilnahme an dieser Studie! </strong></p>

<p>Sie haben die Studie jetzt erfolgreich abgeschlossen. Bitte klicken Sie auf 
 <a href="%(prolific_url)s">HIER</a>
 um diese Studie an Prolific zurückzuschicken und damit Ihre Zahlung sicherzustellen.
</p> 
<p></p>
<p>Wir konnten Sie in der zugewiesenen Zeit nicht mit anderen Personen gruppieren. Daher können Sie mit der Studie nicht fortfahren. Wir
 möchte Ihnen Ihre Zeit erstatten.</p>
<p><strong>Klicken Sie dafür bitte auf diesen Link, um zur FEELE-Zahlungs-App zu gelangen: <a href="%(fwd_url)s">FEELE-Zahlung</a></strong>

</p>

 
<p><strong>Klicken Sie bitte auf diesen Link, um zur FEELE-Zahlungs-App zu gelangen: <a href="%(fwd_url)s">FEELE-Zahlung</a></strong></p>
    
 
<p>Sind Sie derzeit Studierender/Studierende an einer Universität/Hochschule?</p>
 
<p>Sind Sie derzeit Student/Studentin an einer Universität/Hochschule?</p>
 
<p>Sind Sie derzeit Student an einer Universität/Hochschule?</p>
 
<p>Bitte geben Sie für die folgenden Aussagen auf einer Skala von 1 bis 5 an, inwieweit Sie der Aussage zustimmen 
 (wobei 1 "stimme überhaupt nicht zu" und 5 "stimme voll zu"  bedeutet).</p>
 
 <p>In welchem Land leben Sie derzeit?</p>
 
<p>Bitte senden Sie diese Studie an Prolific zurück und wir bezahlen Sie über die Bonusfunktion.</p>
 
<p>Vielen Dank, dass Sie sich heute Zeit genommen und an unserem Experiment teilgenommen haben.</p>
<p>Wir zahlen an die von Ihnen genannte E-Mail-Adresse <strong>"%(email)s"</strong> in den kommenden Tagen.</p>
 
<p>Danke, dass Sie sich heute Zeit genommen haben an dieser Studie teilzunehmen.</p>
<p>Sie haben an 5 Stufen teilgenommen, und wie zu Beginn des Experiments beschrieben, haben wir zufällig eine dieser Stufen für Ihren Abschlusszahlungsbonus ausgewählt.</p>



<table class="table table-sm table-striped">
     <tr>
       <td>Bonus Stufe</td>
       <td>%(bonus_stage)s</td>
     </tr>
     <tr>
       <td>Bonus Bezahlung</td>
       <td>%(bonus_payment)s</td>
     </tr>
     <tr>
       <td>Stufe 1 Zusatzeinkommen</td>
       <td>Bitte beachten Sie, dass dieses<br>Zusatzeinkommen berechnet wird,<br>nachdem die Studie abgeschlossen wurde.</td>
     </tr>
     <tr>
       <td>Stufe 2 Zusatzeinkommen</td>
       <td>%(stage2_q_bonus)s</td>
     </tr>
    <tr>
        <td>Zusatzeinkommen Instruktionen</td>
        <td>%(gender_bonus)s ECU</td>
    </tr>
     <tr>
       <td>
       <h3>Gesamtbonuszahlung</h3>
       </td>
       <td>
       <h3>%(total)s</h3>
       </td>
     </tr>
</table>
 
<p>Leider hat ein Mitglied Ihrer Gruppe nicht mit der vorgegebenen Seite "%(pagename)s" in der zugewiesenen Zeit interagiert,
 und daher die Studie abgebrochen.</p>
<p>Das ist unglaublich frustrierend für uns und Sie. Wir werden Ihnen %(compensation)s als Ausgleich für Ihre Zeit bezahlen.</p>

<p>Vielen Dank, dass Sie sich heute Zeit genommen haben. Es tut uns leid, dass Sie die Studie nicht beenden konnten.</p>

<p>Bitte kehren Sie zu Prolific zurück und wir bezahlen Sie über die Bonusfunktion.</p>

 
<p>Leider haben Sie den Bedingungen der Studie nicht zugestimmt. Deshalb können Sie nicht an unserem Experiment teilnehmen und werden nicht bezahlt</p>

 
<p>Leider haben Sie mit der Seite: <strong>"%(pagename)s"</strong> nicht in der zugewiesenen Zeit interagiert.
 Sie haben die Studie daher abgebrochen und werden nicht bezahlt.</p>
<p>Das ist unglaublich frustrierend für uns. Wir müssen die andere Mitglieder Ihrer Gruppe für ihre Zeit bezahlen und
verlieren alle Daten!</p>

 
<p>Wir brauchen <b>Ihre E-Mail-Adresse, über die wir Sie bezahlen können</b>. Dies Information wird nicht mit den Entscheidungen verknüpft
 die Sie heute gemacht haben, oder verknüpft mit den experimentellen Daten, die  aufgezeichnet wurden. Sie wird für ausschließlich
 für die Bezahlung dieses Experiments verwendet.</p>
 
<p>Wir konnten Sie in der zugewiesenen Zeit nicht mit anderen Personen gruppieren. Daher können Sie nicht mit der Studie fortfahren. Wir
 möchte Sie für Ihre bisher investierte Zeit entschädigen. Dies muss über die Bonusfunktion auf Prolific erfolgen. Also müssen Sie zur Umfrage auf Prolific zurückkehren, 
 damit wir diese Zahlung durchführen können. Bitte senden Sie uns eine Nachricht über das Prolific-System und 
 teilen Sie uns so schnell wie möglich mit, dass Sie auf Prolific zurückgekehrt sind. Vielen Dank für Ihre Zeit.</p>
 

<p>Wir konnten Sie in der zugewiesenen Zeit nicht mit anderen Personen gruppieren. Daher können Sie mit der Studie nicht fortfahren. Wir
 möchte Ihnen Ihre bisher aufgewendete Zeit mit 3 Euro erstatten.</p>

<p>Wir brauchen dafür <b>Ihre E-Mail-Adresse, über die wir Sie bezahlen können</b>. Diese Information wird nicht mit den Entscheidungen,
 die Sie heute gemacht haben, oder mit den experimentellen Daten, die  aufgezeichnet wurden, verknüpft. Sie wird ausschließlich für 
 die Bezahlung dieser Studie verwendet.</p>
 ... und zum Schluss, lassen Sie uns bitte noch ein Feedback zu Ihren Erfahrungen zukommen, die Sie mit diesem Experiment hatten. Ist Ihr direkter Vorgesetzter bei der Arbeit weiblich oder männlich? Ist Ihr direkter Vorgesetzter/Ihre direkte Vorgesetzte bei der Arbeit weiblich oder männlich? Ist die Ihnen vorgesetzte Person bei der Arbeit weiblich oder männlich? Stimme<br>vollkommen zu Stimme<br>überhaupt nicht zu Bitte bestätigen Sie Ihre E-Mail-Adresse Fanden Sie einen oder mehrere Teile der Studie seltsam? Oder haben Sie sich  über einen oder mehrere Teile gewundert? Glauben Sie, dass einige Teile der Studie zusammenhängen? Wenn ja, was war die Verbindung? Studienabbruch Studienabbruchsopfer E-Mail-Adresse Weiblich Hat irgendetwas, das Sie in einem Teil getan haben, Ihr Verhalten oder Ihre Leistung in einem anderen Teil beeinflusst? Wenn ja, wie genau war Ihr Verhalten (Ihre Leistung) betroffen? Ich fühlte mich in meiner Gruppe aufgenommen. Ich fühlte mich weniger berechtigt als andere Teilnehmer der Gruppenleiter in Stufe 5 zu sein. Ich fühlte mich weniger berechtigt als andere Teilnehmer/Teilnehmerinnen der Gruppenleiter/die Gruppenleiterin in Stufe 5 zu sein. Ich fühlte mich weniger berechtigt als andere Teilnehmende die gruppenleitende Person in Stufe 5 zu sein. Ich fühlte mich weniger berechtigt als andere Teilnehmer an dem Wettbewerb in Stufe 3 teilzunehmen. Ich fühlte mich weniger berechtigt als andere Teilnehmer/Teilnehmerinnen an dem Wettbewerb in Stufe 3 teilzunehmen. Ich fühlte mich weniger berechtigt als andere Teilnehmende an dem Wettbewerb in Stufe 3 teilzunehmen. Ich fühlte mich, als würde ich zu meiner Gruppe gehören. Ich fühlte mich als gleichberechtigtes Mitglied der Gruppe. Ich habe meine Entscheidungen in dieser Studie sorgfältig getroffen Ich habe die Entscheidungen in dieser Studie nach dem Zufallsprinzip getroffen Ich fand es unangemessen in Stufe 5 der Gruppenleiter zu sein. Ich fand es unangemessen in Stufe 5 der Gruppenleiter/die Gruppenleiterin zu sein. Ich fand es unangemessen in Stufe 5 die gruppenleitende Person zu sein. Ich fand es unangebracht am Wettbewerb in Stufe 3 teilzunehmen. Ich habe verstanden, was meine Entscheidungen für meine Bezahlung bedeuten Männlich Keine Antwort Nein Keine Einwilligung Divers Bitte klicken Sie auf die Option ganz links Bitte klicken Sie auf die Option ganz rechts Bitte denken Sie an die Personen in Ihrem Leben, die für Sie Vorbilder sind. Welcher Anteil von ihnen ist weiblich versus männlich? Bitte denken Sie an die Personen in Ihrem Leben, die für Sie Vorbilder sind. Welcher Anteil von ihnen ist weiblich versus männlich? Bitte denken Sie an die Personen in Ihrem Leben, die für Sie Vorbilder sind. Welcher Anteil von ihnen ist weiblich versus männlich? Bitte geben Sie an, inwieweit Sie den folgenden Aussagen zustimmen Fragebogen - Seite 1 Fragebogen - Seite 2 Fragebogen - Seite 3 Fragebogen - Seite 4 Fragebogen - Seite 5 Ergebnisse Vielen Dank Inwieweit unterstützen Sie diese Partei? Was denken Sie, worum es in dieser Studie ging? Welcher ist Ihr höchster Bildungsabschluss? Welche Partei wählen Sie normalerweise? Ja Ihre Gruppe wurde nicht gebildet. Ihre E-Mail-Adressen stimmen nicht überein.  