from otree.api import Currency as c, currency_range

from settings import DROPOUT_STATES_DROPOUT, DROPOUT_STATES_NON_CONSENT, DROPOUT_STATES_UNGROUPED, DROPOUT_STATES_VICTIM, LANGUAGE_CODE
from ._builtin import Page, WaitPage
from .models import Constants, Player
from random import randint
import logging
from common.utils import get_fwd_url
from django.utils.translation import gettext as _
import re

logger = logging.getLogger(__name__)

# Valid email address regex
regex = re.compile(r'([A-Za-z0-9]+[.\-_])*[A-Za-z0-9]+@[A-Za-z0-9-]+(\.[A-Z|a-z]{2,})+')


def email_validator(email, conf_email):
    # First check they are the same
    if email != conf_email:
        return _("Your email addresses do not match")

    # Then check that they are actually email addresses:
    if not re.fullmatch(regex, email):
        return f"This is an invalid email address : {email}"


class Involvement(Page):
    form_model = 'player'
    form_fields = [
        'involvement_1',
        'involvement_2',
        'involvement_3',
        'involvement_4',
        'involvement_5',
        'involvement_6',
        'involvement_7',
    ]

    def is_displayed(self):
        return self.player.is_playing()


class Questionnaire1(Page):
    form_model = 'player'
    form_fields = ['country_of_residence', 'manager_gender', 'role_model_gender', 'risk']

    def is_displayed(self):
        return self.player.is_playing()


class Questionnaire2(Page):
    form_model = 'player'
    form_fields = ['education', 'vote', 'uni_student', 'party_support']

    def before_next_page(self):
        # Use this method to determine which stage the player is going to get paid for
        self.player.paid_stage = randint(1, 5)

    def is_displayed(self):
        return self.player.is_playing()


class Att(Page):
    form_model = 'player'
    form_fields = [
        'attention_1',
        'attention_2',
        'attention_3',
        'attention_4',
        'attention_5',
    ]

    def is_displayed(self):
        return self.player.is_playing()


class Debrief(Page):
    form_model = 'player'
    form_fields = [
        'debrief_1',
        'debrief_2',
        'debrief_3',
        'debrief_4',
    ]

    def is_displayed(self):
        return self.player.is_playing()


class Feedback(Page):
    form_model = 'player'
    form_fields = ['problems', 'typos', 'comments']

    def is_displayed(self):
        # This page is only required for the English pilot
        if LANGUAGE_CODE == 'en' and self.player.is_playing() and self.session.config['pilot']:
            return True
        return False


class Understanding(Page):
    form_model = 'player'
    form_fields = ['understanding_gender',]

    def is_displayed(self):
        # This page is only required for the German version
        if LANGUAGE_CODE == 'de' and self.player.is_playing():
            return True
        return False

    def before_next_page(self):
        # Use this method to determine which stage the player is going to get paid for
        guess = self.player.understanding_gender
        gender = self.session.config['genderise']
        pay_bonus = False
        logging.info(f"Looking at understanding bonus. Gender: {gender}, guess: {guess}")
        if (gender == "he" and guess == 1):
            pay_bonus = True
        elif (gender == "she" and guess == 2):
            pay_bonus = True
        elif (gender == "they" and guess == 3):
            pay_bonus = True

        if pay_bonus:
            self.player.understanding_gender_bonus = Constants.understanding_gender_bonus


class Results(Page):
    form_model = 'player'

    # The email address is only required if payment is being processed manually
    def get_form_fields(self):
        if self.session.config['payment_method'] == 'email':
            return ['email', 'email_confirmation']
        else:
            return []

    def vars_for_template(self):
        stage2_q_bonus = c(0)
        payoff_stage_name = 's' + str(self.player.paid_stage) + "_payoff"
        # This is only required so I can test the 'completion' app standalone without running the main stages
        if payoff_stage_name not in self.player.participant.vars:
            self.player.participant.vars[payoff_stage_name] = 12

        bonus = self.player.participant.vars[payoff_stage_name]
        self.player.payoff = bonus
        if 'stage2_perf' in self.player.participant.vars:
            stage2_q_bonus = self.player.participant.vars['stage2_perf']

        if LANGUAGE_CODE == 'en':
            prolific_url = "https://app.prolific.co/submissions/complete?cc=326204E5"
            gender_bonus = 0
        else:
            prolific_url = "https://app.prolific.co/submissions/complete?cc=77FC02A1"
            gender_bonus = self.player.understanding_gender_bonus

        total = bonus + stage2_q_bonus + gender_bonus
        self.player.participant.payoff = total

        return dict(
            gender_bonus=gender_bonus,
            bonus_stage=self.player.paid_stage,
            bonus_payment=bonus,
            stage2_q_bonus=stage2_q_bonus,
            total=total,
            prolific_url=prolific_url,
            fwd_url=get_fwd_url(self.player.participant.payoff_plus_participation_fee(),
                                (self.participant.label or self.participant.code))
        )

    def is_displayed(self):
        if self.player.is_playing():
            self.player.status = "Finished"
            return True

    def error_message(self, values):
        if self.session.config['payment_method'] == 'email':
            return email_validator(values['email'], values['email_confirmation'])


class Dropout(Page):
    def is_displayed(self):
        log("Dropout", self.player)
        if not self.player.is_playing():
            if self.player.participant.vars['dropout_state'] == DROPOUT_STATES_DROPOUT:
                self.player.status = DROPOUT_STATES_DROPOUT
                return True

    def vars_for_template(self):
        return dict(pagename=self.player.participant.vars['dropout_info'])


class DropoutVictim(Page):
    form_model = 'player'

    # The email address is only required if payment is being processed manually
    def get_form_fields(self):
        if self.session.config['payment_method'] == 'email':
            return ['email', 'email_confirmation']
        else:
            return []

    def is_displayed(self):
        log("DropoutVictim", self.player)
        if not self.player.is_playing():
            if self.player.participant.vars['dropout_state'] == DROPOUT_STATES_VICTIM:
                self.player.status = DROPOUT_STATES_VICTIM
                return True

    def vars_for_template(self):
        return dict(pagename=self.player.participant.vars['dropout_info'],
                    compensation=self.player.session.config['participation_fee'],
                    fwd_url=get_fwd_url(self.player.session.config['participation_fee'], (self.participant.label or self.participant.code)))

    def error_message(self, values):
        if self.session.config['payment_method'] == 'email':
            return email_validator(values['email'], values['email_confirmation'])


class NonConsent(Page):
    def is_displayed(self):
        log("NonConsent", self.player)
        if not self.player.is_playing():
            if self.player.participant.vars['dropout_state'] == DROPOUT_STATES_NON_CONSENT:
                self.player.status = DROPOUT_STATES_NON_CONSENT
                return True


class Ungrouped(Page):
    form_model = 'player'

    # The email address is only required if payment is being processed manually
    def get_form_fields(self):
        if self.session.config['payment_method'] == 'email':
            return ['email', 'email_confirmation']
        else:
            return []

    def is_displayed(self):
        log("Ungrouped", self.player)
        if not self.player.is_playing():
            if self.player.participant.vars['dropout_state'] == DROPOUT_STATES_UNGROUPED:
                self.player.status = DROPOUT_STATES_UNGROUPED
                self.player.participant.payoff = 0
                return True

    def vars_for_template(self):
        return dict(fwd_url=get_fwd_url(self.player.session.config['participation_fee'], (self.participant.label or self.participant.code)))

    def error_message(self, values):
        if self.session.config['payment_method'] == 'email':
            return email_validator(values['email'], values['email_confirmation'])


def log(page: str, player: Player) -> None:
    logger.info(
        f"{page}: for player: {(player.participant.label or player.participant.code)}, status: {player.participant.vars['dropout_state']}")


class ThankYou(Page):
    def is_displayed(self):
        # This page is oinly displayed if we are harvesting email addresses for payment, and only shown to dropout victims,
        # people that finished and those that were ungrouped
        if self.session.config['payment_method'] == 'email':
            if self.player.is_playing():
                return True
            elif self.player.participant.vars['dropout_state'] == DROPOUT_STATES_UNGROUPED or self.player.participant.vars['dropout_state'] == DROPOUT_STATES_VICTIM:
                return True

        else:
            return False


page_sequence = [Involvement, Questionnaire1, Questionnaire2, Att, Debrief, Feedback, Understanding,
                 Results, Dropout, DropoutVictim, NonConsent, Ungrouped, ThankYou]
