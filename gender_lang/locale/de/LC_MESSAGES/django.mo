��    m      �  �   �      @	    A	  W  a  7  �  �   �  �   t  �    �  �  �  �   �   o"    X#  	  ^$    h%  -  p&  A  �*  5  �.  �   3  |  �3  �  uB  �  QQ  �  �_    �b    f  �   i  Q   	j    [j  	  ak    kl  V   sm  Z   �m  W   %n  �   }n  �   ao  �   Ep  �   (q  �  
r  G  �t  �  >w  �  �x  �  �z  �  u|  /  ~  .  F  -  u�  5  ��  4  ق  3  �  �   B�     ��     �     �     �     :�     C�  "   Q�  2   t�  3   ��  4   ۆ  /   �  0   @�  1   q�  ,   ��  -   Ї  .   ��  ,   -�  -   Z�  .   ��  ;   ��  <   �  =   0�  B   n�  C   ��  D   ��  C   :�  D   ~�  E   Ê  <   	�  =   F�  >   ��     Ë  
   ��  
   �  �   ��  �   ��  �   !�  M   ��  N   �  O   U�  '   ��  (   ͎  )   ��      �  V   ;�  W   ��  X   �     C�     I�     P�     e�     }�     ��  3   ��     Ր     ې     �     ��  X   �  Y   f�  Z   ��  "  �  O  >�  �  ��  a  ��  �   �  �   {�  �   �    
�  �  �     �    �  9  $�    ^�  M  s�  �  ��  =  ��  �   ��  m  ��  �  ��  �  ��  9  2�  �  l�    *�  �   9�  r   �  �   v�    r�  �   ��  g   ��  }   ��  m   h�  �   ��  �   ��  <  �  �   � 9  � �   �  � �  �
 �  d �  ( .  " :  Q *  � 8  � E  � 5  6 �   l    6 
   >    I &   U    |    � '   � ;   � L   � >   9 ;   x L   � >    8   @ I   y ;   � 8   � I   8 ;   � B   � V    I   X G   � [   � N   F N   � b   � U   G I   � ]   � P   E !   �    �    � �   � �   �  �   P! c   
" �   n" o   �" ,   f# <   �# /   �# 1    $ j   2$ z   �$ m   %    �%    �%    �%    �%    �%    �% ;   �% 
   3& 
   >& 
   I& 
   T& i   _& y   �& l   C'    >       @   O   b       Y       C   (           8   ?                         P              k                    g           h   9   R   !      ;   c           3   V          /                 N   l   6   M           *   &   J          f          [         D      G   .       $      '       \   7      0   ^          j   4   d   I   W          -   T   5       A   )       X   
                  #   <   E       +   K      m   %   =       B   Z          S   F       2   i   ,   U      a   e   	      _   1                "   H              ]   :       L   Q   `    

<p>As in stages 1 and 2, each participant has <strong>2 minutes</strong> to correctly solve as many matrices as
    possible. However, the participant now must <strong>choose</strong> for himself which
    <strong>payment scheme</strong> he prefers for his performance in Stage 3. He can choose either the piece-rate
    payout (as in Stage 1) or the competition payout (as in Stage 2).
</p>

<p>If Stage 3 is the stage selected for payout (from stages 1-5), then a participant's payoff will be determined as
    follows.</p>
<ul>
    <li>If he chooses the <strong>piece-rate</strong> payout, he receives <strong>%(piece_rate)s ECU for each
            correct matrix</strong>.
    </li>
    <li>If he chooses the <strong>competition</strong> payout, his performance will be evaluated in comparison to the
        performance of the other three participants in his group <strong>in Stage 2 (competition)</strong>. If he has
        solved more matrices correctly than the other three participants in his group did in Stage 2, then he will
        receive <strong>%(competition_rate)s ECU for each correct answer</strong>.
        In the event of a tie, the ranking will be decided at random.
    </li>
</ul>

<p>Incorrectly solving a matrix does <strong>not</strong> reduce the number of correctly solved matrices (but it will
    also not increase that number).</p>

<p>The group composition is the same as in Stage 2. If a participant chooses the competition
    payout, he will not be informed of the result of the competition until the study is completed.</p>

 

<p>As in stages 1 and 2, each participant has <strong>2 minutes</strong> to correctly solve as many matrices as
    possible. However, the participant now must <strong>choose</strong> for himself/herself which
    <strong>payment scheme</strong> he/she prefers for his/her performance in Stage 3. He/She can choose either the piece-rate
    payout (as in Stage 1) or the competition payout (as in Stage 2).
</p>

<p>If Stage 3 is the stage selected for payout (from stages 1-5), then a participant's payoff will be determined as
    follows.</p>
<ul>
    <li>If he/she chooses the <strong>piece-rate</strong> payout, he/she receives <strong>%(piece_rate)s ECU for each
            correct matrix</strong>.
    </li>
    <li>If he/she chooses the <strong>competition</strong> payout, his/her performance will be evaluated in comparison to the
        performance of the other three participants in his/her group <strong>in Stage 2 (competition)</strong>. If he/she has
        solved more matrices correctly than the other three participants in his/her group did in Stage 2, then he/she will
        receive <strong>%(competition_rate)s ECU for each correct answer</strong>.
        In the event of a tie, the ranking will be decided at random.
    </li>
</ul>

<p>Incorrectly solving a matrix does <strong>not</strong> reduce the number of correctly solved matrices (but it will
    also not increase that number).</p>

<p>The group composition is the same as in Stage 2. If a participant chooses the competition
    payout, he/she will not be informed of the result of the competition until the study is completed.</p>

 

<p>As in stages 1 and 2, each participant has <strong>2 minutes</strong> to correctly solve as many matrices as
    possible. However, the participant now must <strong>choose</strong> for themselves which
    <strong>payment scheme</strong> they prefer for their performance in Stage 3. They can choose either the piece-rate
    payout (as in Stage 1) or the competition payout (as in Stage 2).
</p>

<p>If Stage 3 is the stage selected for payout (from stages 1-5), then a participant's payoff will be determined as
    follows.</p>
<ul>
    <li>If they choose the <strong>piece-rate</strong> payout, they receive <strong>%(piece_rate)s ECU for each
            correct matrix</strong>.
    </li>
    <li>If they choose the <strong>competition</strong> payout, their performance will be evaluated in comparison to the
        performance of the other three participants in their group <strong>in Stage 2 (competition)</strong>. If they
        have solved more matrices correctly than the other three participants in their group did in Stage 2, then they
        will receive <strong>%(competition_rate)s ECU for each correct answer</strong>. In the event of a tie, the ranking
        will be decided at random.
    </li>
</ul>

<p>Incorrectly solving a matrix does <strong>not</strong> reduce the number of correctly solved matrices (but it will
    also not increase that number).</p>

<p>The group composition is the same as in Stage 2. If a participant chooses the competition
    payout, they will not be informed of the result of the competition until the study is completed.</p>

 

<p>Would you prefer to be paid as in <strong>Stage 1 (piece-rate)</strong> or as in <strong>Stage 2 (competition)?</strong></p>
 
            <th></th>
            <th>Position</th>
            <th>Code</th>
            <th>Gender</th>
            <th>Performance<br>Stage 4</th>
             
<p><strong>Division into groups: Each group consists of 4 participants (2 men and 2 women)</strong>. The group is randomly formed at the
    beginning of this stage, and <strong>each participant stays in the same group until the end of the
        experiment   </strong>.</p>

<p><strong>You will not find out who the other participants in your group are either during or after the experiment, so
        all decisions will remain anonymous</strong>.</p>
 
<p><strong>Division into groups: Each group consists of 4 participants (2 men and 2 women)</strong>. The group is randomly formed at the
    beginning of this stage, and <strong>each participant stays in the same group until the end of the
        experiment  </strong>.</p>

<p><strong>You will not find out who the other participants in your group are either during or after the experiment, so
        all decisions will remain anonymous</strong>.</p>
 
<p><strong>Division into groups: Each group consists of 4 participants (2 men and 2 women)</strong>. The group is randomly formed at the
    beginning of this stage, and <strong>each participant stays in the same group until the end of the
        experiment</strong>.</p>

<p><strong>You will not find out who the other participants in your group are either during or after the experiment, so
        all decisions will remain anonymous</strong>.</p>
 
<p>After you have completed the trial round, you may have to wait briefly before you are able to progress to the
    payoff-relevant (non-trial) task. You will then be given further instructions about the payoff-relevant task.</p>
 
<p>As in Stage 1, each participant has <strong>2 minutes</strong> to correctly solve as many matrices as possible.
    However, the participant's payout for this stage depends on his performance relative to the performance of a group
    of participants.</p>

 
<p>As in Stage 1, each participant has <strong>2 minutes</strong> to correctly solve as many matrices as possible.
    However, the participant's payout for this stage depends on his/her performance relative to the performance of a group
    of participants.</p>

 
<p>As in Stage 1, each participant has <strong>2 minutes</strong> to correctly solve as many matrices as possible.
    However, the participant's payout for this stage depends on their performance relative to the performance of a group
    of participants.</p>

 
<p>As in stages 1-3, each participant has <strong>2 minutes</strong> to solve as many matrices as possible. The
    composition of the group is still the same as before. However, each participant's payout for this stage depends on
    his performance as well as on the performance of all other members of his group.</p>

<p>Specifically, the profit of a participant is determined as follows: He receives
    <strong>%(collective_rate)s ECU</strong> for each correct matrix that a participant in his group has solved in
    the 2 minutes. This means that each correct matrix is worth <strong>%(collective_group)s ECU for the group
        together</strong> (i.e., all 4 members together). This also means that every participant in this stage receives
    the same payout, and this only depends on the <strong>total performance of all participants in the group</strong>,
    i.e., <strong>on the sum of all correct matrices of the 4 participants in the group</strong>. The payout will not be
    reduced if he (or another participant) solves a matrix incorrectly.
</p>

 
<p>As in stages 1-3, each participant has <strong>2 minutes</strong> to solve as many matrices as possible. The
    composition of the group is still the same as before. However, each participant's payout for this stage depends on
    his/her performance as well as on the performance of all other members of his/her group.</p>

<p>Specifically, the profit of a participant is determined as follows: He/She receives
    <strong>%(collective_rate)s ECU</strong> for each correct matrix that a participant in his/her group has solved in
    the 2 minutes. This means that each correct matrix is worth <strong>%(collective_group)s ECU for the group
        together</strong> (i.e., all 4 members together). This also means that every participant in this stage receives
    the same payout, and this only depends on the <strong>total performance of all participants in the group</strong>,
    i.e., <strong>on the sum of all correct matrices of the 4 participants in the group</strong>. The payout will not be
    reduced if he/she (or another participant) solves a matrix incorrectly.
</p>

 
<p>As in stages 1-3, each participant has <strong>2 minutes</strong> to solve as many matrices as possible. The
    composition of the group is still the same as before. However, each participant's payout for this stage depends on
    their performance as well as on the performance of all other members of their group.</p>

<p>Specifically, the profit of a participant is determined as follows: They receive
    <strong>%(collective_rate)s ECU</strong> for each correct matrix that a participant in their group has solved in
    the 2 minutes. This means that each correct matrix is worth <strong>%(collective_group)s ECU for the group
        together</strong> (i.e., all 4 members together). This also means that every participant in this stage receives
    the same payout, and this only depends on the <strong>total performance of all participants in the group</strong>,
    i.e., <strong>on the sum of all correct matrices of the 4 participants in the group</strong>. The payout will not be
    reduced if they (or another participant) solve a matrix incorrectly.
</p>

 
<p>At the end of the study, you will be informed about the overall performance of your group. This then results in your
    payout from this stage.</p>

<p>Click on the Next button, and Stage 4 will start automatically.</p>
 
<p>Each participant remains in the same group of four as before. Each participant in the group now receives an
    identification code and keeps his identification code for the remainder of the experiment. The codes are randomly
    assigned to the players in the group, and are P1, P2, P3 and P4.</p>

<p>For this stage, the number of matrices solved by only one participant in 2 minutes, the
    <strong>"group-leader"</strong>, will be used to determine the payout for the entire group. That is,
    <strong>he solves the task on behalf of the entire group</strong>. Specifically, the
    payouts are calculated as follows:
</p>


<p class="indented">The group-leader receives <strong>%(piece_rate)s ECU per correctly solved matrix</strong>, plus
    a fixed <strong>bonus of %(group_leader_bonus)s ECU</strong> (regardless of performance).
</p>

<p class="indented">The participants of the group who are not group-leaders receive <strong>%(piece_rate)s ECU
        per matrix that the group-leader correctly solved</strong>.</p>

<p>The group-leader is determined through an anonymous election, as follows</p>
<ul>
    <li><u>Candidates</u>: Every participant (P1, P2, P3 or P4) in the group can stand as a group-leader: At the bottom
        of this screen, every participant is asked whether he would like to stand for election.</li>
    <li><u>Voting</u>: Each participant must then order the other three participants in his group on
        the next page. The ordering should reflect his preferences about who is best suited as a group-leader. For
        example, if a participant has been assigned the code P2, then the participant should move this player to the
        top of the table on the next page. He must then decide who his second preference is, and move them to the
        middle of the table. His least favoured participant should be moved to the bottom of the table.</li>
    <li>3 points are awarded to the participant at the top of the table, 2 points to the participant in second place,
        and 1 point to the participant in third place.</li>
    <li>During the voting process, each participant's code and gender is displayed, as well as his performance in Stage
        4 (this is the stage that you have just completed).</li>
    <li>The points awarded are then aggregated across all participants in the group to determine the
        <strong>group-leader</strong>. The group-leader is the participant who has received the
        <strong>most approval points</strong> - assuming he previously decided to stand for election. In other words,
        the group-leader will be the one who, among the candidates, gets the most points in the vote. If there is a tie
        in the points, a group-leader will be determined randomly from among the participants with the same points.
    </li>
    <li>If no participant is standing for election as group-leader, then he will be chosen randomly.</li>
    <li><strong>Every participant who puts himself forward as a candidate has to complete the matrix task</strong>
        after the vote. Whether or not his performance counts for the payoff calculations for the whole group depends on
        the outcome of the vote. If he wins the election, his performance will determine the payoff for the group. If he
        does not win, his performance will not matter. Note, however, that the result of the vote will not be announced
        until the end of the study.</li>
</ul>

<p>The group leader incorrectly solving a matrix does <strong>not</strong> reduce the number of correctly solved
    matrices (but it will also not increase that number).</p>

<p>Now each participant must decide whether he wants to stand for election.</p>
 
<p>Each participant remains in the same group of four as before. Each participant in the group now receives an
    identification code and keeps his/her identification code for the remainder of the experiment. The codes are
    randomly assigned to the players in the group, and are P1, P2, P3 and P4.</p>

<p>For this stage, the number of matrices solved by only one participant in 2 minutes, the
    <strong>"group-leader"</strong>, will be used to determine the payout for the entire group. That is,
    <strong>he/she solves the task on behalf of the entire group</strong>. Specifically, the
    payouts are calculated as follows:
</p>


<p class="indented">The group-leader receives <strong>%(piece_rate)s ECU per correctly solved matrix</strong>, plus
    a fixed <strong>bonus of %(group_leader_bonus)s ECU</strong> (regardless of performance).
</p>

<p class="indented">The participants of the group who are not group-leaders receive <strong>%(piece_rate)s ECU
        per matrix that the group-leader correctly solved</strong>.</p>

<p>The group-leader is determined through an anonymous election, as follows</p>
<ul>
    <li><u>Candidates</u>: Every participant (P1, P2, P3 or P4) in the group can stand as a group-leader: At the bottom
        of this screen, every participant is asked whether he/she would like to stand for election.</li>
    <li><u>Voting</u>: Each participant must then order the other three participants in his/her group on
        the next page. The ordering should reflect his/her preferences about who is best suited as a group-leader. For
        example, if a participant has been assigned the code P2, then the participant should move this player to the
        top of the table on the next page. He/She must then decide who his/her second preference is, and move them to
        the middle of the table. His/Her least favoured participant should be moved to the bottom of the table.</li>
    <li>3 points are awarded to the participant at the top of the table, 2 points to the participant in second place,
        and 1 point to the participant in third place.</li>
    <li>During the voting process, each participant's code and gender is displayed, as well as his/her performance in Stage
        4 (this is the stage that you have just completed).</li>
    <li>The points awarded are then aggregated across all participants in the group to determine the
        <strong>group-leader</strong>. The group-leader is the participant who has received the
        <strong>most approval points</strong> - assuming he/she previously decided to stand for election. In other
        words, the group-leader will be the one who, among the candidates, gets the most points in the vote. If there is
        a tie in the points, a group-leader will be determined randomly from among the participants with the same
        points.
    </li>
    <li>If no participant is standing for election as group-leader, then he/she will be chosen randomly.</li>
    <li><strong>Every participant who puts themselves forward as a candidate has to complete the matrix task</strong>
        after the vote. Whether or not his/her performance counts for the payoff calculations for the whole group
        depends on
        the outcome of the vote. If he/she wins the election, his/her performance will determine the payoff for the
        group. If
        he/she does not win, his/her performance will not matter. Note, however, that the result of the vote will not be
        announced until the end of the study.</li>
</ul>

<p>The group leader incorrectly solving a matrix does <strong>not</strong> reduce the number of correctly solved
    matrices (but it will also not increase that number).</p>

<p>Now each participant must decide whether he/she wants to stand for election.</p>
 
<p>Each participant remains in the same group of four as before. Each participant in the group now receives an
    identification code and keeps their identification code for the remainder of the experiment. The codes are randomly
    assigned to the players in the group, and are P1, P2, P3 and P4.</p>

<p>For this stage, the number of matrices solved by only one participant in 2 minutes, the
    <strong>"group-leader"</strong>, will be used to determine the payout for the entire group. That is,
    <strong>they solve the task on behalf of the entire group</strong>. Specifically, the
    payouts are calculated as follows:
</p>


<p class="indented">The group-leader receives <strong>%(piece_rate)s ECU per correctly solved matrix</strong>, plus
    a fixed <strong>bonus of %(group_leader_bonus)s ECU</strong> (regardless of performance).
</p>

<p class="indented">The participants of the group who are not group-leaders receive <strong>%(piece_rate)s ECU
        per matrix that the group-leader correctly solved</strong>.</p>

<p>The group-leader is determined through an anonymous election, as follows</p>
<ul>
    <li><u>Candidates</u>: Every participant (P1, P2, P3 or P4) in the group can stand as a group-leader: At the bottom
        of this screen, every participant is asked whether they would like to stand for election.</li>
    <li><u>Voting</u>: Each participant must then order the other three participants in their group on
        the next page. The ordering should reflect their preferences about who is best suited as a group-leader. For
        example, if a participant has been assigned the code P2, then the participant should move this player to the
        top of the table on the next page. They must then decide who their second preference is, and move them to the
        middle of the table. Their least favoured participant should be moved to the bottom of the table.</li>
    <li>3 points are awarded to the participant at the top of the table, 2 points to the participant in second place,
        and 1 point to the participant in third place.</li>
    <li>During the voting process, each participant's code and gender is displayed, as well as their performance in Stage
        4 (this is the stage that you have just completed).</li>
    <li>The points awarded are then aggregated across all participants in the group to determine the
        <strong>group-leader</strong>. The group-leader is the participant who has received the
        <strong>most approval points</strong> - assuming they previously decided to stand for election. In other words,
        the group-leader will be the one who, among the candidates, gets the most points in the vote. If there is a tie
        in the points, a group-leader will be determined randomly from among the participants with the same points.
    </li>
    <li>If no participant is standing for election as group-leader, then they will be chosen randomly.</li>
    <li><strong>Every participant who puts themselves forward as a candidate has to complete the matrix task</strong>
        after the vote. Whether or not their performance counts for the payoff calculations for the whole group depends
        on the outcome of the vote. If they win the election, their performance will determine the payoff for the group.
        If they do not win, their performance will not matter. Note, however, that the result of the vote will not be
        announced until the end of the study.</li>
</ul>

<p>The group leader incorrectly solving a matrix does <strong>not</strong> reduce the number of correctly solved
    matrices (but it will also not increase that number).</p>

<p>Now each participant must decide whether they want to stand for election.</p>
 
<p>If Stage 2 is the stage selected for payout, then a participant's payoff will depend on how many matrices he has
    correctly solved compared to the other three participants in his group. The payout will not be
    reduced if he (or another participant) solves a matrix incorrectly.</p>

<p>The participant who has the largest number of correct matrices is the <strong>winner of the competition</strong>. He
    receives <strong>%(competition_rate)s ECU for each correct matrix</strong>, while the other participants in
    his group <strong>do not receive any payout</strong>. The winning participant is determined randomly if there is a
    tie between two participants. From now on, we will call the payout from this stage the “competition payout.”
</p>

 
<p>If Stage 2 is the stage selected for payout, then a participant's payoff will depend on how many matrices he/she has
    correctly solved compared to the other three participants in his/her group. The payout will not be
    reduced if he/she (or another participant) solves a matrix incorrectly.</p>

<p>The participant who has the largest number of correct matrices is the <strong>winner of the competition</strong>. He/She
    receives <strong>%(competition_rate)s ECU for each correct matrix</strong>, while the other participants in
    his/her group <strong>do not receive any payout</strong>. The winning participant is determined randomly if there is a
    tie between two participants. From now on, we will call the payout from this stage the “competition payout.”
</p>

 
<p>If Stage 2 is the stage selected for payout, then a participant's payoff will depend on how many matrices they have
    correctly solved compared to the other three participants in their group. The payout will not be
    reduced if they (or another participant) solve a matrix incorrectly.</p>

<p>The participant who has the largest number of correct matrices is the <strong>winner of the competition</strong>.
    They receive <strong>%(competition_rate)s ECU for each correct matrix</strong>, while the other participants in
    their group <strong>do not receive any payout</strong>. The winning participant is determined randomly if there is a
    tie between two participants. From now on, we will call the payout from this stage the “competition payout.”
</p>

 
<p>Incorrectly solving a matrix does <strong>not</strong> reduce the number of correctly solved matrices (but it will
    also not increase that number)."</p>
    
<p>When you are ready, please click on "Next" and Stage 1 will start automatically.</p>
 
<p>Make a choice, then click "Next". Then Stage 3 will start automatically.</p>
 
<p>Now Stage 1 of the study (piece-rate payout) takes place, in which each participant <strong>receives
        %(piece_rate)s ECU for each</strong> of his <strong>correctly solved matrices</strong>. You
    have <strong>2 minutes</strong> for this task.</p>

 
<p>Now Stage 1 of the study (piece-rate payout) takes place, in which each participant <strong>receives
        %(piece_rate)s ECU for each</strong> of his/her <strong>correctly solved matrices</strong>. You
    have <strong>2 minutes</strong> for this task.</p>

 
<p>Now Stage 1 of the study (piece-rate payout) takes place, in which each participant <strong>receives
        %(piece_rate)s ECU for each</strong> of their <strong>correctly solved matrices</strong>. You
    have <strong>2 minutes</strong> for this task.</p>

 
<p>Now each participant must decide how he wants to be paid for the next stage.</p>

 
<p>Now each participant must decide how he/she wants to be paid for the next stage.</p>

 
<p>Now each participant must decide how they want to be paid for the next stage.</p>

 
<p>On the next screen, you will be asked whether you want to choose the piece-rate payout or the competition payout for
    your performance in Stage 3. You will then have <strong>2 minutes</strong> to solve the matrices.</p>
 
<p>Which participant should be the group-leader? For each of the three participants in your group, please
    move the participant up or down in the table using the arrows.  </p>
<p>Note, you have the code %(gender_code)s</p>
 
<p>Which participant should be the group-leader? For each of the three participants in your group, please
    move the participant up or down in the table using the arrows. </p>
<p>Note, you have the code %(gender_code)s</p>
 
<p>Which participant should be the group-leader? For each of the three participants in your group, please
    move the participant up or down in the table using the arrows.</p>
<p>Note, you have the code %(gender_code)s</p>
 
<p>You are about to enter a trial round and will have <strong>1 minute</strong> to familiarize yourself with the task.
    The trial round is not payoff relevant.</p>

<p>Below is a screenshot of the matrix like the one you will be interacting with.</p>

<p>Your task is to find <strong>two numbers in the given matrix that add up to 100</strong>. When you have found two
    numbers, select these numbers, and click on "Submit" to display the next matrix. After clicking the Submit button,
    you will always see how many matrices you have solved correctly and incorrectly.</p>

<p>Incorrectly solving a matrix does <strong>not</strong> reduce the number of correctly solved matrices (but it will
    also not increase that number).</p>
&nbsp;
 
<p>You are now in the trial round and have 1 minute to familiarize yourself with the task.</p>

<p>Your task is to find two numbers in the given matrix that add up to 100. When you have found two numbers, select
    these numbers, and click on "Confirm" to display the next matrix. After clicking the confirmation button, you will
    always see how many matrices you have solved correctly and incorrectly.</p>

<p>Incorrectly solving a matrix does <strong>not</strong> reduce the number of correctly solved matrices (but it will
    also not increase that number).</p>
    
&nbsp;
 
<p>You are participant %(gender_code)s  </p>

<table class="table table-sm table-striped">
    <thead class="thead-dark">
        <tr>
            <th>Stage</th>
            <th># Correct</th>
            <th># Attempts</th>
            <th>S2 Bonus</th>
            <th>S3 Mode</th>
            <th>S4 Ordering</th>
            <th>S5 Standing</th>
            <th>S5 GL</th>
            <th>Payoff</th>
        </tr>
    </thead>
    <tbody>
 
<p>You are participant %(gender_code)s </p>

<table class="table table-sm table-striped">
    <thead class="thead-dark">
        <tr>
            <th>Stage</th>
            <th># Correct</th>
            <th># Attempts</th>
            <th>S2 Bonus</th>
            <th>S3 Mode</th>
            <th>S4 Ordering</th>
            <th>S5 Standing</th>
            <th>S5 GL</th>
            <th>Payoff</th>
        </tr>
    </thead>
    <tbody>
 
<p>You are participant %(gender_code)s</p>

<table class="table table-sm table-striped">
    <thead class="thead-dark">
        <tr>
            <th>Stage</th>
            <th># Correct</th>
            <th># Attempts</th>
            <th>S2 Bonus</th>
            <th>S3 Mode</th>
            <th>S4 Ordering</th>
            <th>S5 Standing</th>
            <th>S5 GL</th>
            <th>Payoff</th>
        </tr>
    </thead>
    <tbody>
 
<p>You have <strong>1 minute</strong> to familiarize yourself with the task.</p>

<p>Your task is to find <strong>two numbers in the given matrix that add up to 100</strong>. When you have found two
    numbers, select these numbers, and click on "Submit" to display the next matrix. After clicking the Submit button,
    you will always see how many matrices you have solved correctly and incorrectly.</p>

&nbsp;
 
<p>You now have the opportunity again to earn an <strong>additional income of %(s2_bonus)s ECU</strong>. To do
    this, you need to correctly answer the following question:  </p>
<p>How do you think your performance in Stage 2 compared to the performance of the other participants in your group?</p>

 
<p>You now have the opportunity again to earn an <strong>additional income of %(s2_bonus)s ECU</strong>. To do
    this, you need to correctly answer the following question: </p>
<p>How do you think your performance in Stage 2 compared to the performance of the other participants in your group?</p>

 
<p>You now have the opportunity again to earn an <strong>additional income of %(s2_bonus)s ECU</strong>. To do
    this, you need to correctly answer the following question:</p>
<p>How do you think your performance in Stage 2 compared to the performance of the other participants in your group?</p>

 
<p>You now have the opportunity to earn an <strong>additional income of %(s1_bonus)s ECU</strong>. To do this,
    you need to correctly answer the following question:  </p>

<p>How do you think did your performance in Stage 1 compare to the performance of the other participants in the
    experiment?</p>

 
<p>You now have the opportunity to earn an <strong>additional income of %(s1_bonus)s ECU</strong>. To do this,
    you need to correctly answer the following question: </p>

<p>How do you think did your performance in Stage 1 compare to the performance of the other participants in the
    experiment?</p>

 
<p>You now have the opportunity to earn an <strong>additional income of %(s1_bonus)s ECU</strong>. To do this,
    you need to correctly answer the following question:</p>

<p>How do you think did your performance in Stage 1 compare to the performance of the other participants in the
    experiment?</p>

 
<p>You will not be informed of the outcome of the competition until the study is completed.</p>

<p>When you are ready, click the "Next" button, and Stage 2 will start automatically.</p>
 Choice Competition Confirm Debug - Results from all Stages Election Group payment I don't want to stand for election I think I was among the bottom 25% of participants I think I was among the bottom 25% of participants  I think I was among the bottom 25% of participants   I think I was in the bottom 50% of participants I think I was in the bottom 50% of participants  I think I was in the bottom 50% of participants   I think I was in the top 25% of participants I think I was in the top 25% of participants  I think I was in the top 25% of participants   I think I was in the top 50% of participants I think I was in the top 50% of participants  I think I was in the top 50% of participants   I think I was the best of the four participants in my group I think I was the best of the four participants in my group  I think I was the best of the four participants in my group   I think I was the second-best of the four participants in my group I think I was the second-best of the four participants in my group  I think I was the second-best of the four participants in my group   I think I was the second-worst of the four participants in my group I think I was the second-worst of the four participants in my group  I think I was the second-worst of the four participants in my group   I think I was the worst of the four participants in my group I think I was the worst of the four participants in my group  I think I was the worst of the four participants in my group   I want to stand for election Piece Rate Piece-rate Please wait for other participants to finish reading the instructions, and then you will be grouped. This can take up to 8 min so please be patient. Please wait for other participants to finish reading the instructions, and then you will be grouped. This can take up to 8 min so please be patient.  Please wait for other participants to finish reading the instructions, and then you will be grouped. This can take up to 8 min so please be patient.   Please wait for the other participants to finish voting for the group-leader. Please wait for the other participants to finish voting for the group-leader.  Please wait for the other participants to finish voting for the group-leader.   Please wait for the other participants. Please wait for the other participants.  Please wait for the other participants.   Please wait to be grouped. Please wait while the participants that opted to stand for election complete the task. Please wait while the participants that opted to stand for election complete the task.  Please wait while the participants that opted to stand for election complete the task.   Quiz  Stage  Stage 1 - Piece Rate Stage 1 is now finished Stage 2 is now finished Start trial The instructions for Stage 1 will appear shortly... Trial Trial round Trial round  Trial round instructions When you have decided on the participants' ordering, please click on the Confirm button. When you have decided on the participants' ordering, please click on the Confirm button.  When you have decided on the participants' ordering, please click on the Confirm button.   Project-Id-Version: 
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2022-07-08 08:59+0100
Last-Translator: 
Language-Team: 
Language: de
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 3.0.1
 

<p>Wie in Stufe 1 und 2 hat jeder Teilnehmer <strong>2 Minuten</strong> Zeit, um so viele Matrizen wie möglich 
 richtig zu lösen. Jeder Teilnehmer muss jetzt selbst <strong>auswählen</strong>, welchen
 <strong>Zahlungsmodus</strong> er für seine Leistung in Stufe 3 bevorzugt. Er kann entweder die Stück-Quote Bezahlung wählen
 (wie in Stufe 1) oder die Wettbewerbsauszahlung (wie in Stufe 2).
</p>

<p>Wenn Stufe 3 die für die Auszahlung ausgewählte Stufe ist (von den Stufen 1-5), dann wird die Auszahlung für jeden Teilnehmer 
wie folgt bestimmt.</p>
<ul>
 <li>Wählt er die <strong>Stück-Quote</strong>-Auszahlung, erhält er <strong>%(piece_rate)s ECU für jede
 richtig gelöste Matrix.</strong>
 </li>
 <li>Wenn er sich für die <strong>Wettbewerbsauszahlung</strong> entscheidet, wird seine Leistung im Vergleich zur 
 Leistung der anderen drei Teilnehmer in seiner Gruppe <strong>in Stufe 2 (Wettbewerb)</strong> bewertet. Wenn er
 mehr Matrizen richtig gelöst hat als die anderen drei Teilnehmer in seiner Gruppe in Stufe 2, dann erhält er
<strong>%(competition_rate)s ECU für jede richtige Antwort</strong>.
 Im Falle eines Unentschiedens wird der Gewinner per Zufall bestimmt.
 </li>
</ul>

<p>Das falsche Lösen einer Matrix reduziert <strong>nicht</strong> die Anzahl der richtig gelösten Matrizen (aber es 
 erhöht diese Anzahl auch nicht).</p>

<p>Die Gruppenzusammensetzung ist die Gleiche wie in Stufe 2. Wenn ein Teilnehmer sich für die Wettbewerbsauszahlung entscheidet,
dann wird er erst über das Ergebnis des Wettbewerbs informiert, wenn die Studie abgeschlossen ist.</p>

 

<p>Wie in Stufe 1 und 2 hat jeder Teilnehmer/jede Teilnehmerin <strong>2 Minuten</strong> Zeit, um so viele Matrizen wie möglich 
 richtig zu lösen. Jeder Teilnehmer/Jede Teilnehmerin muss jetzt selbst <strong>auswählen</strong>, welchen
 <strong>Zahlungsmodus</strong> er/sie für seine/ihre Leistung in Stufe 3 bevorzugt. Er/Sie kann entweder die Stück-Quote Bezahlung wählen
 (wie in Stufe 1) oder die Wettbewerbsauszahlung (wie in Stufe 2).
</p>

<p>Wenn Stufe 3 die für die Auszahlung ausgewählte Stufe ist (von den Stufen 1-5), dann wird die Auszahlung für jeden Teilnehmer/jede Teilnehmerin 
wie folgt bestimmt.</p>
<ul>
 <li>Wählt er/sie die <strong>Stück-Quote</strong>-Auszahlung, erhält er/sie <strong>%(piece_rate)s ECU für jede
 richtig gelöste Matrix.</strong>
 </li>
 <li>Wenn er/sie sich für die <strong>Wettbewerbsauszahlung</strong> entscheidet, wird seine/ihre Leistung im Vergleich zur 
 Leistung der anderen drei Teilnehmer/Teilnehmerinnen in seiner/ihrer Gruppe <strong>in Stufe 2 (Wettbewerb)</strong> bewertet. Wenn er/sie
 mehr Matrizen richtig gelöst hat als die anderen drei Teilnehmer/Teilnehmerinnen in seiner/ihrer Gruppe in Stufe 2, dann erhält er/sie 
<strong>%(competition_rate)s ECU für jede richtige Antwort</strong>.
 Im Falle eines Unentschiedens wird der Gewinner/die Gewinnerin per Zufall bestimmt.
 </li>
</ul>

<p>Das falsche Lösen einer Matrix reduziert <strong>nicht</strong> die Anzahl der richtig gelösten Matrizen (aber es 
 erhöht die Anzahl auch nicht).</p>

<p>Die Gruppenzusammensetzung ist die Gleiche wie in Stufe 2. Wenn ein Teilnehmer/eine Teilnehmerin sich für die Wettbewerbsauszahlung entscheidet,
dann wird er/sie erst über das Ergebnis des Wettbewerbs informiert, wenn die Studie abgeschlossen ist.</p>

 

<p>Wie in Stufe 1 und 2 haben alle Teilnehmenden <strong>2 Minuten</strong> Zeit, um so viele Matrizen wie möglich 
 richtig zu lösen. Teilnehmende müssen jetzt selbst <strong>auswählen</strong>, welchen
 <strong>Zahlungsmodus</strong> sie für die Leistung in Stufe 3 bevorzugen. Man kann entweder die Stück-Quote Bezahlung wählen
 (wie in Stufe 1) oder die Wettbewerbsauszahlung (wie in Stufe 2).
</p>

<p>Wenn Stufe 3 die für die Auszahlung ausgewählte Stufe ist (von den Stufen 1-5), dann wird die Auszahlung für alle Teilnehmenden 
wie folgt bestimmt.</p>
<ul>
 <li>Wählt man die <strong>Stück-Quote</strong>-Auszahlung, erhält man <strong>%(piece_rate)s ECU für jede
 richtig gelöste Matrix.</strong>
 </li>
 <li>Wenn man sich für die <strong>Wettbewerbsauszahlung</strong> entscheidet, wird die Leistung im Vergleich zur 
 Leistung der anderen drei Teilnehmenden in der Gruppe <strong>in Stufe 2 (Wettbewerb)</strong> bewertet. Wenn man
 mehr Matrizen richtig gelöst hat als die anderen drei Teilnehmenden in der Gruppe in Stufe 2, dann erhält man 
<strong>%(competition_rate)s ECU für jede richtige Antwort</strong>.
 Im Falle eines Unentschiedens wird die gewinnende Person per Zufall bestimmt.
 </li>
</ul>

<p>Das falsche Lösen einer Matrix reduziert <strong>nicht</strong> die Anzahl der richtig gelösten Matrizen (aber es 
 erhöht die Anzahl auch nicht).</p>

<p>Die Gruppenzusammensetzung ist die Gleiche wie in Stufe 2. Wenn Teilnehmende sich für die Wettbewerbsauszahlung entscheiden,
dann werden sie erst über das Ergebnis des Wettbewerbs informiert, wenn die Studie abgeschlossen ist.</p>

 

<p>Möchten Sie lieber wie in <strong>Stufe 1 (Stück-Quote)</strong> oder wie in <strong>Stufe 2 (Wettbewerb)</strong> bezahlt werden?</p>
 
            <th></th>
            <th>Position</th>
            <th>Code</th>
            <th>Geschlecht</th>
            <th>Leistung<br>Stufe 4</th>
             
<p><strong>Einteilung in Gruppen: Jede Gruppe besteht aus 4 Teilnehmenden (zwei Männer und zwei Frauen)</strong>. Die Gruppe wird nach dem Zufallsprinzip 
 am Beginn dieser Stufe gebildet, und <strong>alle Teilnehmenden bleiben in der gleichen Gruppe bis zum Ende des
 Experiments.</strong></p>

<p><strong>Sie erfahren nicht, wer die anderen Teilnehmenden Ihrer Gruppe sind (weder während noch nach dem Experiment). Das bedeutet, dass
 alle Entscheidungen anonym bleiben.</strong></p>
 
<p><strong>Einteilung in Gruppen: Jede Gruppe besteht aus 4 Teilnehmern/Teilnehmerinnen (zwei Männer und zwei Frauen)</strong>. Die Gruppe wird nach dem Zufallsprinzip 
 am Beginn dieser Stufe gebildet, und <strong>jeder Teilnehmer/jede Teilnehmerin bleibt in der gleichen Gruppe bis zum Ende des
 Experiments.</strong></p>

<p><strong>Sie erfahren nicht, wer die anderen Teilnehmer/Teilnehmerinnen Ihrer Gruppe sind (weder während noch nach dem Experiment). Das bedeutet, dass
 alle Entscheidungen anonym bleiben.</strong></p>
 
<p><strong>Einteilung in Gruppen: Jede Gruppe besteht aus 4 Teilnehmern (zwei Männer und zwei Frauen)</strong>. Die Gruppe wird nach dem Zufallsprinzip 
 am Beginn dieser Stufe gebildet, und <strong>jeder Teilnehmer bleibt in der gleichen Gruppe bis zum Ende des
 Experiments.</strong></p>

<p><strong>Sie erfahren nicht, wer die anderen Teilnehmer Ihrer Gruppe sind (weder während noch nach dem Experiment). Das bedeutet, dass
 alle Entscheidungen anonym bleiben.</strong></p>
 
<p>Nachdem Sie die Proberunde abgeschlossen haben müssen Sie eventuell kurz warten, bevor Sie 
 zur nächsten auszahlungsrelevanten Aufgabe (keine-Proberunde mehr) weitergeleitet werden. Sie werden dann weitere Anleitungen zur auszahlungsrelevanten Aufgabe erhalten.</p>
 
<p>Wie in Stufe 1 hat jeder Teilnehmer <strong>2 Minuten</strong> Zeit, um so viele Matrizen wie möglich richtig zu lösen.
 Die Auszahlung des Teilnehmers für diese Stufe hängt jedoch von seiner Leistung relativ zur Leistung der anderen
Mitglieder der Gruppe ab.</p>

 
<p>Wie in Stufe 1 hat jeder Teilnehmer/jede Teilnehmerin <strong>2 Minuten</strong> Zeit, um so viele Matrizen wie möglich richtig zu lösen.
 Die Auszahlung des Teilnehmers/der Teilnehmerin für diese Stufe hängt jedoch von seiner/ihrer Leistung relativ zur Leistung der anderen
Mitglieder der Gruppe ab.</p>
 
<p>Wie in Stufe 1 haben alle Teilnehmenden <strong>2 Minuten</strong> Zeit, um so viele Matrizen wie möglich richtig zu lösen.
 Die Auszahlung der Teilnehmenden für diese Stufe hängt jedoch von der Leistung relativ zur Leistung der anderen
Mitglieder der Gruppe ab.</p>

 
<p>Wie in den Stufen 1-3 hat jeder Teilnehmer <strong>2 Minuten</strong> Zeit, um so viele Matrizen wie möglich zu lösen. Die
 Zusammensetzung der Gruppe ist immer noch die Gleiche wie zuvor. Die Auszahlung des Teilnehmers für diese Phase hängt von
 seiner Leistung, sowie von der Leistung aller anderen Mitglieder seiner Gruppe ab.</p>

<p>Im Einzelnen wird die Auszahlung eines Teilnehmers wie folgt ermittelt: Er erhält
 <strong>%(collective_rate)s ECU</strong> für jede richtige Matrix, die ein Teilnehmer in seiner Gruppe in
2 Minuten gelöst hat. Das bedeutet, dass jede richtige Matrix <strong>%(collective_group)s ECU für die Gruppe
 gesamt bringt</strong> (d.h. für alle 4 Mitglieder zusammen). Dies bedeutet auch, dass jeder Teilnehmer in dieser Stufe
die gleiche Auszahlung erhält, und diese hängt nur von der <strong>Gesamtleistung aller Teilnehmer in der Gruppe</strong> ab,
 d.h. <strong>von der Summe aller richtigen Matrizen der 4 Teilnehmer in der Gruppe</strong>. Die Auszahlung wird nicht
 reduziert, wenn er (oder ein anderer Teilnehmer) eine Matrix falsch löst.
</p>

 
<p>Wie in den Stufen 1-3 hat jeder Teilnehmer/jede Teilnehmerin <strong>2 Minuten</strong> Zeit, um so viele Matrizen wie möglich zu lösen. Die
 Zusammensetzung der Gruppe ist immer noch die Gleiche wie zuvor. Die Auszahlung des Teilnehmers/der Teilnehmerin für diese Phase hängt von
 seiner/ihrer Leistung, sowie von der Leistung aller anderen Mitglieder seiner/ihrer Gruppe ab.</p>

<p>Im Einzelnen wird die Auszahlung eines Teilnehmers/einer Teilnehmerin wie folgt ermittelt: Er/Sie erhält
 <strong>%(collective_rate)s ECU</strong> für jede richtige Matrix, die ein Teilnehmer/eine Teilnehmerin in seiner/ihrer Gruppe in
2 Minuten gelöst hat. Das bedeutet, dass jede richtige Matrix <strong>%(collective_group)s ECU für die Gruppe
 gesamt bringt</strong> (d.h. für alle 4 Mitglieder zusammen). Dies bedeutet auch, dass jeder Teilnehmer/jede Teilnehmerin in dieser Stufe
die gleiche Auszahlung erhält, und diese hängt nur von der <strong>Gesamtleistung aller Teilnehmer/Teilnehmerinnen in der Gruppe</strong> ab,
 d.h. <strong>von der Summe aller richtigen Matrizen der 4 Teilnehmer/Teilnehmerinnen in der Gruppe</strong>. Die Auszahlung wird nicht
 reduziert, wenn er/sie (oder ein anderer Teilnehmer/eine andere Teilnehmerin) eine Matrix falsch löst.
</p>

 
<p>Wie in den Stufen 1-3 haben alle Teilnehmenden <strong>2 Minuten</strong> Zeit, um so viele Matrizen wie möglich zu lösen. Die
 Zusammensetzung der Gruppe ist immer noch die Gleiche wie zuvor. Die Auszahlung der Teilnehmenden für diese Phase hängt von
der eigenen Leistung, sowie von der Leistung aller anderen Mitglieder der Gruppe ab.</p>

<p>Im Einzelnen wird die Auszahlung der Teilnehmenden wie folgt ermittelt: Man erhält
 <strong>%(collective_rate)s ECU</strong> für jede richtige Matrix, die ein Mitglied der Gruppe in
2 Minuten gelöst hat. Das bedeutet, dass jede richtige Matrix <strong>%(collective_group)s ECU für die Gruppe
 gesamt bringt</strong> (d.h. für alle 4 Mitglieder zusammen). Dies bedeutet auch, dass alle Teilnehmenden in dieser Stufe
die gleiche Auszahlung erhalten, und diese hängt nur von der <strong>Gesamtleistung aller Teilnehmenden in der Gruppe</strong> ab,
 d.h. <strong>von der Summe aller richtigen Matrizen der 4 Teilnehmenden in der Gruppe</strong>. Die Auszahlung wird nicht
 reduziert, wenn jemand eine Matrix falsch löst.
</p>

 
<p>Am Ende der Studie werden Sie über die Gesamtleistung Ihrer Gruppe informiert. Daraus ergibt sich dann Ihre
 Auszahlung aus dieser Stufe.</p>

 
<p>Jeder Teilnehmer bleibt in der gleichen Vierergruppe wie zuvor. Jeder Teilnehmer der Gruppe erhält jetzt einen
 Identifikationscode und behält seinen Identifikationscode für den Rest des Experiments. Die Codes sind zufällig
 den Spielern in der Gruppe zugewiesen und lauten P1, P2, P3 und P4.</p>

<p>Für diese Stufe ist die Anzahl der richtigen Matrizen von nur einem Teilnehmer der Gruppe (<strong>dem "Gruppenleiter"</strong>) entscheidend. 
 <strong>Der Gruppenleiter</strong> hat 2 Minuten Zeit, um Matrizen zu lösen, die die Auszahlung für die gesamte Gruppe bestimmen. Das heißt,
 <strong>er löst die Matrizen stellvertretend für die gesamte Gruppe</strong>. Die
 Auszahlungen werden wie folgt berechnet:
</p>


<p class="indented">Der Gruppenleiter erhält <strong>%(piece_rate)s ECU pro korrekt gelöster Matrix</strong>, plus einem <strong>fixen Bonus von %(group_leader_bonus)s ECU</strong> (unabhängig von der Leistung).

</p>

<p class="indented">Die Teilnehmer der Gruppe, die keine Gruppenleiter sind, erhalten <strong>%(piece_rate)s ECU
 pro
 Matrix, die der Gruppenleiter richtig gelöst hat</strong>.</p>

<p>Der Gruppenleiter wird durch eine anonyme Wahl wie folgt bestimmt: </p>
<ul>
 <li><u>Kandidaten</u>: Jeder Teilnehmer (P1, P2, P3 oder P4) in der Gruppe kann sich am Ende dieses Bildschirms
als Gruppenleiter zur Wahl stellen:
 <li><u>Wahl</u>: Jeder Teilnehmer muss dann am nächsten Bildschirm die anderen drei Teilnehmer in seiner Gruppe 
 reihen. Die Reihenfolge sollte Ihre Vorlieben darüber widerspiegeln, wer für Sie am besten als Gruppenleiter geeignet ist. Zum
 Beispiel: Wenn Sie einen Teilnehmer am besten finden,  dann sollte dieser Teilnehmer auf der nächsten Seite
 an die Tabellenspitze verschoben werden. Dann müssen Sie entscheiden, wer ihre zweite Präferenz ist. Verschieben Sie diesen Teilnehmer zur
Mitte der Tabelle. Ihr unbeliebtester Teilnehmer sollte an das Ende der Tabelle verschoben werden.</li>
 <li>3 Punkte erhält der Teilnehmer an der Tabellenspitze, 2 der zweitgereihte Teilnehmer,
 und 1 Punkt für den Teilnehmer auf dem dritten (letzten) Platz.</li>
 <li>Während Sie die Reihung vornehmen, werden der Code und das Geschlecht jedes Teilnehmers, sowie seine Leistung in Stufe 4 (dies
 ist die Stufe, die Sie gerade abgeschlossen haben), angezeigt.</li>
 <li>Die vergebenen Punkte werden dann über alle Teilnehmer aggregiert und bestimmen 
 <strong>den Gruppenleiter</strong>. Der Gruppenleiter ist der Teilnehmer der 
 <strong>die meisten Punkte</strong> erhalten hat - vorausgesetzt, er hat sich zur Wahl gestellt. Mit anderen Worten,
 der Gruppenleiter wird derjenige sein, der unter allen Kandidaten die meisten Punkte bei der Abstimmung bekommen hat. Bei Punktegleichstand
wird ein Gruppenleiter per Zufall aus den Teilnehmern mit der höchsten gleichen Punktezahl ermittelt.
 </li>
 <li>Wenn sich kein Teilnehmer zur Wahl als Gruppenleiter stellt, dann wird er zufällig ausgewählt.</li>
 <li><strong>Jeder Teilnehmer, der sich als Kandidat zur Wahl stellt, muss die Matrixaufgabe </strong>
 nach der Abstimmung lösen. Ob seine Leistung für die Auszahlung der gesamten Gruppe zählt oder nicht, hängt vom
 Ergebnis der Abstimmung ab. Wenn er die Wahl gewinnt, bestimmt seine Leistung  die Auszahlung für die Gruppe. Wenn
er die Wahl nicht gewinnt, spielt seine Leistung keine Rolle. Beachten Sie jedoch: das Ergebnis der Abstimmung wird
 erst am Ende der Studie bekannt gegeben.</li>
</ul>

<p>Der Gruppenleiter, der eine Matrix falsch löst, verringert <strong>nicht</strong> die Anzahl der richtig gelösten Matrizen.

<p>Nun muss jeder Teilnehmer entscheiden, ob er sich zur Wahl stellen will.</p>
 
<p>Jeder Teilnehmer/Jede Teilnehmerin bleibt in der gleichen Vierergruppe wie zuvor. Jeder Teilnehmer/Jede Teilnehmerin der Gruppe erhält jetzt einen
 Identifikationscode und behält seinen/ihren Identifikationscode für den Rest des Experiments. Die Codes sind zufällig
 den Spielern/Spielerinnen in der Gruppe zugewiesen und lauten P1, P2, P3 und P4.</p>

<p>Für diese Stufe ist die Anzahl der richtigen Matrizen von nur einem Teilnehmer/einer Teilnehmerin der Gruppe (<strong>dem "Gruppenleiter"/"der Gruppenleiterin"</strong>) entscheidend.
 <strong>Der Gruppenleiter/Die Gruppenleiterin</strong> hat 2 Minuten Zeit um Matrizen zu lösen, die die Auszahlung für die gesamte Gruppe bestimmen. Das heißt,
 <strong>er/sie löst die Matrizen stellvertretend für die gesamte Gruppe</strong>. Die
 Auszahlungen werden wie folgt berechnet:
</p>


<p class="indented">Der Gruppenleiter/Die Gruppenleiterin erhält <strong>%(piece_rate)s ECU pro korrekt gelöster Matrix</strong>, plus einem <strong>fixen Bonus von %(group_leader_bonus)s ECU</strong> (unabhängig von der Leistung).
</p>

<p class="indented">Die Teilnehmer/Die Teilnehmerinnen der Gruppe, die keine Gruppenleiter/Gruppenleiterinnen sind, erhalten <strong>%(piece_rate)s ECU
 pro
 Matrix, die der Gruppenleiter/die Gruppenleiterin richtig gelöst hat</strong>.</p>

<p>Der Gruppenleiter/Die Gruppenleiterin wird durch eine anonyme Wahl wie folgt bestimmt: </p>
<ul>
 <li><u>Kandidaten/Kandidatinnen</u>: Jeder Teilnehmer/Jede Teilnehmerin (P1, P2, P3 oder P4) in der Gruppe kann sich am Ende dieses Bildschirms
als Gruppenleiter/Gruppenleiterin zur Wahl stellen:
 <li><u>Wahl</u>: Jeder Teilnehmer/Jede Teilnehmerin muss dann am nächsten Bildschirm die anderen drei Teilnehmer/Teilnehmerinnen in seiner/ihrer Gruppe 
 reihen. Die Reihenfolge sollte Ihre Vorlieben darüber widerspiegeln, wer für Sie am besten als Gruppenleiter/Gruppenleiterin geeignet ist. Zum
 Beispiel: Wenn Sie einen Teilnehmer/eine Teilnehmerin am besten finden,  dann sollte dieser Teilnehmer/diese Teilnehmerin auf der nächsten Seite
 an die Tabellenspitze verschoben werden. Dann müssen Sie entscheiden, wer ihre zweite Präferenz ist. Verschieben Sie diesen Teilnehmer/diese Teilnehmerin zur
Mitte der Tabelle. Ihr unbeliebtester Teilnehmer/Ihre unbeliebteste Teilnehmerin sollte an das Ende der Tabelle verschoben werden.</li>
 <li>3 Punkte erhält der Teilnehmer/die Teilnehmerin an der Tabellenspitze, 2 der zweitgereihte Teilnehmer/die zweitgereihte Teilnehmerin,
 und 1 Punkt für den Teilnehmer/die Teilnehmerin auf dem dritten (letzten) Platz.</li>
 <li>Während Sie die Reihung vornehmen, werden der Code und das Geschlecht jedes Teilnehmers/jeder Teilnehmerin, sowie seine/ihre Leistung in Stufe 4 (dies
 ist die Stufe, die Sie gerade abgeschlossen haben), angezeigt.</li>
 <li>Die vergebenen Punkte werden dann über alle Teilnehmer/Teilnehmerinnen aggregiert und bestimmen 
 <strong>den Gruppenleiter/die Gruppenleiterin</strong>. Der Gruppenleiter/Die Gruppenleiterin ist der Teilnehmer/die Teilnehmerin der/die 
 <strong>die meisten Punkte</strong> erhalten hat - vorausgesetzt, er/sie hat sich zur Wahl gestellt. Mit anderen Worten,
 der Gruppenleiter/die Gruppenleiterin wird derjenige/diejenige sein, der/die unter allen Kandidaten/Kandidatinnen die meisten Punkte bei der Abstimmung bekommen hat. Bei Punktegleichstand
wird ein Gruppenleiter/eine Gruppenleiterin per Zufall aus den Teilnehmern/Teilnehmerinnen mit der höchsten gleichen Punktezahl ermittelt.
 </li>
 <li>Wenn sich kein Teilnehmer/keine Teilnehmerin zur Wahl als Gruppenleiter/Gruppenleiterin stellt, dann wird er/sie zufällig ausgewählt.</li>
 <li><strong>Jeder Teilnehmer/Jede Teilnehmerin, der/die sich als Kandidat/Kandidatin zur Wahl stellt, muss die Matrixaufgabe </strong>
 nach der Abstimmung lösen. Ob seine/ihre Leistung für die Auszahlung der  gesamten Gruppe zählt oder nicht, hängt vom
 Ergebnis der Abstimmung ab. Wenn er/sie die Wahl gewinnt, bestimmt seine/ihre Leistung  die Auszahlung für die Gruppe. Wenn 
er/sie die Wahl nicht gewinnt, spielt seine/ihre Leistung keine Rolle. Beachten Sie jedoch: das Ergebnis der Abstimmung wird
 erst am Ende der Studie bekannt gegeben.</li>
</ul>

<p>Der Gruppenleiter/Die Gruppenleiterin, der/die eine Matrix falsch löst, verringert <strong>nicht</strong> die Anzahl der richtig gelösten Matrizen.

<p>Nun muss jeder Teilnehmer/jede Teilnehmerin entscheiden, ob er/sie sich zur Wahl stellen will.</p>
 
<p>Teilnehmende bleiben in der gleichen Vierergruppe wie zuvor. Alle in der Gruppe erhalten jetzt einen
 Identifikationscode und behalten den Identifikationscode für den Rest des Experiments. Die Codes sind zufällig
 den Spielenden in der Gruppe zugewiesen und lauten P1, P2, P3 und P4.</p>

<p>Für diese Stufe ist die Anzahl der richtigen Matrizen von nur einer Person der Gruppe (<strong>der "gruppenleitenden Person"</strong>) entscheidend.
 <strong>Die gruppenleitende Person</strong> hat 2 Minuten Zeit, um Matrizen zu lösen, die die Auszahlung für die gesamte Gruppe bestimmen. Das heißt,
 <strong>man löst die Matrizen stellvertretend für die gesamte Gruppe</strong>. Die
 Auszahlungen werden wie folgt berechnet:
</p>


<p class="indented">Die gruppenleitende Person erhält <strong>%(piece_rate)s ECU pro korrekt gelöster Matrix</strong>, plus einem <strong>fixen Bonus von %(group_leader_bonus)s ECU</strong> (unabhängig von der Leistung).
</p>

<p class="indented">Teilnehmende der Gruppe, die nicht gruppenleitend sind, erhalten <strong>%(piece_rate)s ECU
 pro
 Matrix, die die Person richtig gelöst hat</strong>.</p>

<p>Die gruppenleitende Person wird durch eine anonyme Wahl wie folgt bestimmt: </p>
<ul>
 <li><u>Kandidierende</u>: Alle Teilnehmenden (P1, P2, P3 oder P4) in der Gruppe können sich am Ende dieses Bildschirms
für die Gruppenleitung zur Wahl stellen:
 <li><u>Wahl</u>: Teilnehmende müssen dann am nächsten Bildschirm die anderen drei Teilnehmenden in der Gruppe 
 reihen. Die Reihenfolge sollte Ihre Vorlieben darüber widerspiegeln, wer für Sie am besten als gruppenleitende Person geeignet ist. Zum
 Beispiel: Wenn Sie eine Person am besten finden, dann sollte die Person auf der nächsten Seite
 an die Tabellenspitze verschoben werden. Dann müssen Sie entscheiden, wer ihre zweite Präferenz ist. Verschieben Sie diese Person zur
Mitte der Tabelle. Ihre unbeliebteste Person sollte an das Ende der Tabelle verschoben werden.</li>
 <li>3 Punkte erhält die Person an der Tabellenspitze, 2 die zweitgereihte Person,
 und 1 Punkt für die Person auf dem dritten (letzten) Platz.</li>
 <li>Während Sie die Reihung vornehmen, werden der Code und das Geschlecht der Personen, sowie die Leistung in Stufe 4 (dies
 ist die Stufe, die Sie gerade abgeschlossen haben), angezeigt.</li>
 <li>Die vergebenen Punkte werden dann über alle Teilnehmende aggregiert und bestimmen 
 <strong>die gruppenleitende Person</strong>. Die gruppenleitende Person ist die Person, die 
 <strong>die meisten Punkte</strong> erhalten hat - vorausgesetzt, diese Person hat sich zur Wahl gestellt. Mit anderen Worten,
 die gruppenleitende Person wird die Person sein, die unter allen Kandidierenden die meisten Punkte bei der Abstimmung bekommen hat. Bei Punktegleichstand
wird eine gruppenleitende Person per Zufall
aus den Teilnehmenden mit der höchsten gleichen Punktezahl ermittelt.
 </li>
 <li>Wenn sich keine Person zur Wahl als gruppenleitende Person stellt, dann wird jemand zufällig ausgewählt.</li>
 <li><strong>Teilnehmende, die zur Wahl kandidieren, müssen die Matrixaufgabe </strong>
 nach der Abstimmung lösen. Ob die Leistung für die Auszahlung der gesamten Gruppe zählt oder nicht, hängt vom
 Ergebnis der Abstimmung ab. Wenn jemand die Wahl gewinnt, bestimmt die Leistung die Auszahlung für die Gruppe. Wenn
die Person die Wahl nicht gewinnt, spielt die Leistung keine Rolle. Beachten Sie jedoch: das Ergebnis der Abstimmung wird
 erst am Ende der Studie bekannt gegeben.</li>
</ul>

<p>Die gruppenleitende Person, die eine Matrix falsch löst, verringert <strong>nicht</strong> die Anzahl der richtig gelösten Matrizen.

<p>Nun müssen alle entscheiden, ob sie sich zur Wahl stellen wollen.</p>
 
<p>Wenn Stufe 2 die zur Auszahlung ausgewählte Stufe ist, dann hängt die Auszahlung eines Teilnehmers davon ab, wie viele Matrizen er 
 im Vergleich zu den anderen drei Teilnehmern seiner Gruppe richtig gelöst hat. Die Anzahl der richtig gelösten Matrizen wird nicht
reduziert, wenn er (oder ein anderer Teilnehmer) eine Matrix falsch löst.</p>

<p>Der Teilnehmer, der die meisten Matrizen richtig gelöst hat, ist der <strong>Gewinner des Wettbewerbs</strong>. Er
 erhält <strong>%(competition_rate)s ECU für jede richtige Matrix</strong>, während die anderen Teilnehmer
 seiner Gruppe <strong>keine Auszahlung erhalten</strong>. Der Gewinner wird zufällig bestimmt, wenn es einen
Gleichstand zwischen zwei Teilnehmern gibt. Von nun an nennen wir die Auszahlung von dieser Stufe die "Wettbewerbsauszahlung".
</p>

 
<p>Wenn Stufe 2 die zur Auszahlung ausgewählte Stufe ist, dann hängt die Auszahlung eines Teilnehmers/einer Teilnehmerin davon ab, wie viele Matrizen er/sie 
 im Vergleich zu den anderen drei Teilnehmern/Teilnehmerinnen seiner/ihrer Gruppe richtig gelöst hat. Die Auszahlung wird nicht
reduziert, wenn er/sie (oder ein anderer Teilnehmer/eine andere Teilnehmerin) eine Matrix falsch löst.</p>

<p>Der Teilnehmer/Die Teilnehmerin, der/die die meisten Matrizen richtig gelöst hat, ist <strong>der Gewinner/die Gewinnerin des Wettbewerbs</strong>. Er/Sie
 erhält <strong>%(competition_rate)s ECU für jede richtige Matrix</strong>, während die anderen Teilnehmer/Teilnehmerinnen 
 der Gruppe <strong>keine Auszahlung erhalten</strong>. Der Gewinner/Die Gewinnerin wird zufällig bestimmt, wenn es einen
Gleichstand zwischen zwei Teilnehmern/Teilnehmerinnen gibt. Von nun an nennen wir die Auszahlung von dieser Stufe die "Wettbewerbsauszahlung".
</p>

 
<p>Wenn Stufe 2 die zur Auszahlung ausgewählte Stufe ist, dann hängt die Auszahlung der Teilnehmenden davon ab, wie viele Matrizen 
 im Vergleich zu den anderen drei Teilnehmenden der Gruppe richtig gelöst wurden. Die Auszahlung wird nicht
reduziert, wenn jemand eine Matrix falsch löst.</p>

<p>Die Person, die die meisten Matrizen richtig gelöst hat, <strong>gewinnt den Wettbewerb</strong>. Diese Person 
 erhält <strong>%(competition_rate)s ECU für jede richtige Matrix</strong>, während die anderen Teilnehmenden
 der Gruppe <strong>keine Auszahlung erhalten</strong>. Die Person, die gewinnt, wird zufällig bestimmt, wenn es einen
Gleichstand zwischen zwei Teilnehmenden gibt. Von nun an nennen wir die Auszahlung von dieser Stufe die "Wettbewerbsauszahlung".
</p>

 
<p>Sie werden erst am Ende des Experiments über das Ergebnis des Wettbewerbs informiert.</p>

<p>Wenn Sie bereit sind, klicken Sie auf die Schaltfläche "Weiter" und Stufe 2 beginnt automatisch.</p>
 
<p>Treffen Sie eine Entscheidung und klicken Sie dann auf "Weiter". Dann wird Stufe 3 automatisch gestartet.</p>
 
<p>Nun findet Stufe 1 der Studie (Stück-Quote) statt, in der jeder Teilnehmer <strong>
 %(piece_rate)s ECU für jede</strong> seiner <strong>richtig gelösten Matrizen</strong> erhält. Sie
 haben <strong>2 Minuten</strong> für diese Aufgabe.</p>

 
<p>Nun findet Stufe 1 der Studie (Stück-Quote) statt, in der jeder Teilnehmer/jede Teilnehmerin <strong>
 %(piece_rate)s ECU für jede</strong> seiner/ihrer <strong>richtig gelösten Matrizen</strong> erhält. Sie
 haben <strong>2 Minuten</strong> für diese Aufgabe.</p>

 
<p>Nun findet Stufe 1 der Studie (Stück-Quote) statt, in der alle Teilnehmenden <strong>
 %(piece_rate)s ECU für jede</strong> der <strong>richtig gelösten Matrizen</strong> erhalten. Sie
 haben <strong>2 Minuten</strong> für diese Aufgabe.</p>

 
<p>Nun muss jeder Teilnehmer entscheiden, wie er für die nächste Stufe bezahlt werden möchte.</p>

 
<p>Nun muss jeder Teilnehmer/jede Teilnehmerin entscheiden, wie er/sie für die nächste Stufe bezahlt werden möchte.</p>

 
<p>Nun müssen alle Teilnehmende entscheiden, wie sie für die nächste Stufe bezahlt werden möchten.</p>

 
<p>Auf dem nächsten Bildschirm werden Sie gefragt, ob Sie die Stück-Quote oder die Wettbewerbsauszahlung für
 Ihre Leistung in Stufe 3 möchten. Sie haben dann <strong>2 Minuten</strong> um die Matrizen zu lösen.</p>
 
<p>Welche Person soll die Gruppe leiten? Für jede der drei Personen in Ihrer Gruppe, bitte
    bewegen Sie die Person in der Tabelle mit den Pfeilen nach oben oder unten.</p>
<p>Hinweis, Sie haben den Code %(gender_code)s</p>
 
<p>Welcher Teilnehmer/Welche Teilnehmerin soll der Gruppenleiter/die Gruppenleiterin sein? Für jeden der drei Teilnehmer/Teilnehmerinnen in Ihrer Gruppe, bitte 
 bewegen Sie den Teilnehmer/die Teilnehmerin in der Tabelle mit den Pfeilen nach oben oder unten.</p>
<p>Hinweis, Sie haben den Code %(gender_code)s</p>
 
<p>Welcher Teilnehmer soll der Gruppenleiter sein? Für jeden der drei Teilnehmer in Ihrer Gruppe, bitte 
 bewegen Sie den Teilnehmer in der Tabelle mit den Pfeilen nach oben oder unten.</p>
<p>Hinweis, Sie haben den Code %(gender_code)s</p>
 
<p>Sie sind dabei an der Proberunde teilzunehmen und haben <strong>1 Minute</strong>, um sich mit der Aufgabe vertraut zu machen.
 Die Proberunde ist nicht auszahlungsrelevant.</p>

<p>Unten wird als Beispiel der Screenshot einer Matrix angezeigt.</p>

<p>Ihre Aufgabe ist es <strong>zwei Zahlen in der gegebenen Matrix zu finden, die sich auf 100 aufaddieren</strong>. Wenn Sie zwei Zahlen gefunden haben,
dann wählen Sie diese Zahlen aus und klicken auf "Senden", um die nächste Matrix angezeigt zu bekommen. Nachdem Sie auf die Schaltfläche "Senden" geklickt haben,
sehen Sie immer, wie viele Matrizen Sie richtig gelöst haben und wie viele falsch.</p>

<p>Das falsche Lösen einer Matrix reduziert <strong>nicht</strong> die Anzahl der richtig gelösten Matrizen (aber es erhöht
 die Anzahl auch nicht).</p>
&nbsp;
 
<p>Sie befinden sich jetzt in der Proberunde und haben 1 Minute Zeit, um sich mit der Aufgabe bekannt zu machen.</p>

<p>Ihre Aufgabe ist es, in der gegebenen Matrix zwei Zahlen zu finden, die zusammen 100 ergeben. Wenn Sie zwei Nummern gefunden haben, dann wählen Sie
 diese Zahlen aus (indem Sie auf diese klicken) und klicken auf "Bestätigen", um die nächste Matrix anzuzeigen. Nachdem Sie auf den Bestätigungsbutton geklickt haben, sehen Sie
immer, wie viele Matrizen Sie richtig und falsch gelöst haben.</p>

<p>Das falsche Lösen einer Matrix reduziert <strong>nicht</strong> die Anzahl der korrekt gelösten Matrizen (aber es erhöht diese Anzahl auch nicht).</p>
&nbsp;
 
<p>Sie sind Teilnehmender/Teilnehmende %(gender_code)s  </p>

<table class="table table-sm table-striped">
    <thead class="thead-dark">
        <tr>
            <th>Stufe</th>
            <th># Richtig</th>
            <th># Attempts</th>
            <th>S2 Bonus</th>
            <th>S3 Zahlungsmodus</th>
            <th>S4 Reihung</th>
            <th>S5 Stand</th>
            <th>S5 GL</th>
            <th>Bezahlung</th>
        </tr>
    </thead>
    <tbody>
    
 
<p>Sie sind Teilnehmer/Teilnehmerin %(gender_code)s </p>

<table class="table table-sm table-striped">
    <thead class="thead-dark">
        <tr>
            <th>Stufe</th>
            <th># Richtig</th>
            <th># Attempts</th>
            <th>S2 Bonus</th>
            <th>S3 Zahlungsmodus</th>
            <th>S4 Reihung</th>
            <th>S5 Stand</th>
            <th>S5 GL</th>
            <th>Bezahlung</th>
        </tr>
    </thead>
    <tbody>
    
 
<p>Sie sind Teilnehmer %(gender_code)s</p>

<table class="table table-sm table-striped">
    <thead class="thead-dark">
        <tr>
            <th>Stufe</th>
            <th># Richtig</th>
            <th># Attempts</th>
            <th>S2 Bonus</th>
            <th>S3 Zahlungsmodus</th>
            <th>S4 Reihung</th>
            <th>S5 Stand</th>
            <th>S5 GL</th>
            <th>Bezahlung</th>
        </tr>
    </thead>
    <tbody>
 
<p>Sie haben <strong>1 Minute</strong>, um sich mit der Aufgabe vertraut zu machen.</p>

<p>Ihre Aufgabe ist es <strong>zwei Zahlen in der gegebenen Matrix zu finden, die sich auf 100 aufaddieren</strong>. Wenn Sie zwei Zahlen gefunden haben,
dann wählen Sie diese Zahlen aus und klicken auf "Senden", um die nächste Matrix angezeigt zu bekommen. Nachdem Sie auf die Schaltfläche "Senden" geklickt haben,
sehen Sie immer, wie viele Matrizen Sie richtig gelöst haben und wie viele falsch.</p>

&nbsp;
 
<p>Sie haben jetzt wieder die Möglichkeit ein <strong>Zusatzeinkommen von %(s2_bonus)s ECU</strong> zu verdienen. 
 Dazu müssen Sie die folgende Frage richtig beantworten:</p>
<p>Wie schätzen Sie Ihre Leistung in Stufe 2 im Vergleich zur Leistung der anderen Teilnehmenden in Ihrer Gruppe ein?</p>
 
<p>Sie haben jetzt wieder die Möglichkeit ein <strong>Zusatzeinkommen von %(s2_bonus)s ECU</strong> zu verdienen.
 Dazu müssen Sie die folgende Frage richtig beantworten:</p>
<p>Wie schätzen Sie Ihre Leistung in Stufe 2 im Vergleich zur Leistung der anderen Teilnehmer/Teilnehmerinnen in Ihrer Gruppe ein?</p>
 
<p>Sie haben jetzt wieder die Möglichkeit ein <strong>Zusatzeinkommen von %(s2_bonus)s ECU</strong> zu verdienen.
 Dazu müssen Sie die folgende Frage richtig beantworten:</p>
<p>Wie schätzen Sie Ihre Leistung in Stufe 2 im Vergleich zur Leistung der anderen Teilnehmer in Ihrer Gruppe ein?</p>
 
<p>Sie haben jetzt die Möglichkeit ein <strong>Zusatzeinkommen von %(s1_bonus)s ECU</strong> zu erhalten. Um dies zu tun
 müssen Sie die folgende Frage richtig beantworten:</p>

<p>Wie war Ihrer Meinung nach Ihre Leistung in Stufe 1 im Vergleich zu der Leistung der anderen Teilnehmenden im
 Experiment?</p>

 
<p>Sie haben jetzt die Möglichkeit ein <strong>Zusatzeinkommen von %(s1_bonus)s ECU</strong> zu erhalten. Um dies zu tun
 müssen Sie die folgende Frage richtig beantworten:</p>

<p>Wie war Ihrer Meinung nach Ihre Leistung in Stufe 1 im Vergleich zu der Leistung der anderen Teilnehmer/Teilnehmerinnen im
 Experiment?</p>

 
<p>Sie haben jetzt die Möglichkeit ein <strong>Zusatzeinkommen von %(s1_bonus)s ECU</strong> zu erhalten. Um dies zu tun
 müssen Sie die folgende Frage richtig beantworten:</p>

<p>Wie war Ihrer Meinung nach Ihre Leistung in Stufe 1 im Vergleich zu der Leistung der anderen Teilnehmer im
 Experiment?</p>

 
<p>Sie werden erst am Ende des Experiments über das Ergebnis des Wettbewerbs informiert.</p>

<p>Wenn Sie bereit sind, klicken Sie auf die Schaltfläche "Weiter" und Stufe 2 beginnt automatisch.</p>
 Auswahl Wettbewerb Bestätigen Debuggen - Ergebnisse aus allen Teilen Wahl Gruppe Ich möchte mich nicht zur Wahl stellen Ich glaube, ich gehörte zu den unteren 25 % der Teilnehmer Ich glaube, ich gehörte zu den unteren 25 % der Teilnehmer/Teilnehmerinnen  Ich glaube, ich gehörte zu den unteren 25 % der Teilnehmenden Ich glaube, ich gehörte zu den unteren 50 % der Teilnehmer Ich glaube, ich gehörte zu den unteren 50 % der Teilnehmer/Teilnehmerinnen  Ich glaube, ich gehörte zu den unteren 50 % der Teilnehmenden Ich glaube, ich war unter den besten 25 % der Teilnehmer Ich glaube, ich war unter den besten 25 % der Teilnehmer/Teilnehmerinnen  Ich glaube, ich war unter den besten 25 % der Teilnehmenden Ich glaube, ich war unter den besten 50 % der Teilnehmer Ich glaube, ich war unter den besten 50 % der Teilnehmer/Teilnehmerinnen  Ich glaube, ich war unter den besten 50 % der Teilnehmenden Ich glaube, ich war der beste der vier Teilnehmer in meiner Gruppe Ich glaube, ich war der/die beste der vier Teilnehmer/Teilnehmerinnen in meiner Gruppe Ich glaube, ich war der/die beste der vier Teilnehmenden in meiner Gruppe Ich glaube, ich war der zweitbeste der vier Teilnehmer in meiner Gruppe Ich glaube, ich war der/die zweitbeste der vier Teilnehmer/Teilnehmerinnen in meiner Gruppe Ich glaube, ich war der/die zweitbeste der vier Teilnehmenden in meiner Gruppe Ich glaube, ich war der zweitschlechteste der vier Teilnehmer in meiner Gruppe Ich glaube, ich war der/die zweitschlechteste der vier Teilnehmer/Teilnehmerinnen in meiner Gruppe Ich glaube, ich war der/die zweitschlechteste der vier Teilnehmenden in meiner Gruppe Ich glaube, ich war der schlechteste der vier Teilnehmer in meiner Gruppe Ich glaube, ich war der/die schlechteste der vier Teilnehmer/Teilnehmerinnen in meiner Gruppe Ich glaube, ich war der/die schlechteste der vier Teilnehmenden in meiner Gruppe Ich möchte mich zur Wahl stellen Stück-Quote Stück-Quote Bitte warten Sie, bis die anderen Teilnehmer die Anleitungen zu Ende gelesen haben. Anschließend werden Gruppen gebildet. Das kann bis zu 8 Minuten dauern. Bitte seien Sie geduldig. Bitte warten Sie, bis die anderen Teilnehmer/Teilnehmerinnen die Anleitungen zu Ende gelesen haben. Anschließend werden Gruppen gebildet. Das kann bis zu 8 Minuten dauern. Bitte seien Sie geduldig. Bitte warten Sie, bis die anderen Teilnehmenden die Anleitungen zu Ende gelesen haben. Anschließend werden Gruppen gebildet. Das kann bis zu 8 Minuten dauern. Bitte seien Sie geduldig. Bitte warten Sie, bis die anderen Teilnehmer mit der Abstimmung für den Gruppenleiter fertig sind. Bitte warten Sie, bis die anderen Teilnehmer/Teilnehmerinnen mit der Abstimmung für den Gruppenleiter/die Gruppenleiterin fertig sind. Bitte warten Sie, bis die anderen Teilnehmenden mit der Abstimmung für die gruppenleitende Person fertig sind. Bitte warten Sie auf die anderen Teilnehmer. Bitte warten Sie auf die anderen Teilnehmer/Teilnehmerinnen. Bitte warten Sie auf die anderen Teilnehmenden. Bitte warten Sie, bis Ihre Gruppe gebildet wurde. Bitte warten Sie, bis die Teilnehmer, die sich zur Wahl gestellt haben, die Matrix-Aufgaben gelöst haben. Bitte warten Sie, bis die Teilnehmer/Teilnehmerinnen, die sich zur Wahl gestellt haben, die Matrix-Aufgaben gelöst haben. Bitte warten Sie, bis die Teilnehmenden, die sich zur Wahl gestellt haben, die Matrix-Aufgaben gelöst haben. Quiz  Stufe  Teil 1 - Stück-Quote Stufe 1 ist nun abgeschlossen Teil 2 ist nun abgeschlossen Proberunde starten Die Anleitungen für Stufe 1 werden in Kürze angezeigt ... Proberunde Proberunde Proberunde Proberunde Wenn Sie sich für die Reihenfolge der Teilnehmer entschieden haben, klicken Sie bitte auf "Bestätigen". Wenn Sie sich für die Reihenfolge der Teilnehmer/Teilnehmerinnen entschieden haben, klicken Sie bitte auf "Bestätigen". Wenn Sie sich für die Reihenfolge der Teilnehmenden entschieden haben, klicken Sie bitte auf "Bestätigen". 