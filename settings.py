from os import environ

SESSION_CONFIGS = [
    dict(
        name='the_app_he',
        description="The full app, using his/him/he",
        app_sequence=['consent', 'gender_lang', 'completion', "vceepayment"],
        num_demo_participants=4,
        use_browser_bots=False,
        show_debug_results=False,
        genderise='he',
        pilot=False,
        payment_method="vienna"
    ),
    dict(
        name='the_app_she',
        description="The full app, using hers/her/she",
        app_sequence=['consent', 'gender_lang', 'completion', "vceepayment"],
        num_demo_participants=4,
        use_browser_bots=False,
        show_debug_results=False,
        genderise='she',
        pilot=False,
        payment_method="vienna"
    ),
    dict(
        name='the_app_they',
        description="The full app, using theirs/they/them",
        app_sequence=['consent', 'gender_lang', 'completion', "vceepayment"],
        num_demo_participants=4,
        use_browser_bots=False,
        show_debug_results=False,
        genderise='they',
        pilot=False,
        payment_method="vienna"
    ),
    dict(
        name='TEST_app_with_bots',
        description="TESTING: Just the main matrix game - with bots",
        app_sequence=['consent', 'gender_lang', 'completion', "vceepayment"],
        num_demo_participants=4,
        use_browser_bots=True,
        show_debug_results=True,
        genderise='he',
        pilot=False,
        payment_method="vienna"
    ),
    dict(
        name='TEST_app_no_bots',
        description="TESTING: Just the main matrix game - no bots",
        app_sequence=['gender_lang'],
        num_demo_participants=4,
        use_browser_bots=False,
        show_debug_results=True,
        genderise='he',
        pilot=False,
        payment_method="vienna"
    ),
    dict(
        name='TEST_completion',
        description="TESTING: Just the completion pages",
        app_sequence=['completion'],
        num_demo_participants=4,
        use_browser_bots=False,
        show_debug_results=True,
        genderise='he',
        pilot=False,
        payment_method="email"
    ),
]

# if you set a property in SESSION_CONFIG_DEFAULTS, it will be inherited by all configs
# in SESSION_CONFIGS, except those that explicitly override it.
# the session config can be accessed from methods in your apps as self.session.config,
# e.g. self.session.config['participation_fee']

SESSION_CONFIG_DEFAULTS = dict(
    real_world_currency_per_point=0.00033333, participation_fee=5.00,
    disable_waiting_for_others=True,     # NOTE This is for the VCEE app, so dropouts et al don't have to wait for everyone to finish
    doc="""
    Set the 'payment_method' to one of
        * prolific
        * email
        * uoe
    """
)

DROPOUT_STATES_PLAYING = "Playing"
DROPOUT_STATES_DROPOUT = "Dropout"
DROPOUT_STATES_VICTIM = "Victim"
DROPOUT_STATES_NON_CONSENT = "Non Consent"
DROPOUT_STATES_UNGROUPED = "Ungrouped"


# LANGUAGE_CODE = 'en'
# REAL_WORLD_CURRENCY_CODE = 'GBP'
LANGUAGE_CODE = 'de'
REAL_WORLD_CURRENCY_CODE = 'EUR'

USE_POINTS = True
POINTS_CUSTOM_NAME = 'ECU'

ROOMS = [
    dict(
        name='GL1',
        display_name='Finance and Economics Experimental Laboratory Exeter - 1',
    ),
    dict(
        name='GL2',
        display_name='Finance and Economics Experimental Laboratory Exeter - 2',
    ),
    dict(
        name='GL3',
        display_name='Finance and Economics Experimental Laboratory Exeter - 3',
    ),
    dict(
        name='VCEEroom1',
        display_name='VCEE Room 1'
    ),
    dict(
        name='VCEEroom2',
        display_name='VCEE Room 2'
    ),
    dict(
        name='VCEEroom3',
        display_name='VCEE Room 3'
    ),

]

ADMIN_USERNAME = 'admin'
# for security, best to set admin password in an environment variable
ADMIN_PASSWORD = environ.get('OTREE_ADMIN_PASSWORD')

DEMO_PAGE_INTRO_HTML = """ """

SECRET_KEY = '8021915965807'

# if an app is included in SESSION_CONFIGS, you don't need to list it here
INSTALLED_APPS = ['otree']
