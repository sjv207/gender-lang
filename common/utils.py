
def get_fwd_url(payment, label: str):
    import base64
    import json
    data = {'computername': label, 'payment': float(payment)}

    enc_data = base64.urlsafe_b64encode(json.dumps(data).encode()).decode()
    url = "http://ph.feele.exeter.ac.uk?epd="

    return url + enc_data