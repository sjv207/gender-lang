var active_box1 = 0;
var active_box2 = 0;
var active_sum = 0;
var target_sum = 100;

var clickstream = [];

class Clickstream {
    constructor() {
        this.startTime = Date.now();
        this.clicks = [];
        this.submitTime = null;
        this.submitValue = 0;
    }

    addClick(x, y, value) {
        this.clicks.push(new Click(x, y, value));
    }

    end(value) {
        this.submitValue = value;
        this.submitTime = Date.now();
    }
}

class Click {
    constructor(x, y, value) {
        this.x = x;
        this.y = y;
        this.value = value;
        this.t = Date.now();

        console.log("Added click: [" + x + "," + y + "]: " + value);
    }
}
// Load these 2 values from js vars - stops F5 wiping stuff out
var num_correct = js_vars.num_correct;
var num_total = js_vars.num_total;
console.log("Total: " + num_total + ", correct: " + num_correct + ", language: " + js_vars.language)

// var language = "en"
var language = js_vars.language
var submit_text = (language == "en") ? "Submit" : "Senden";
var solved_text = (language == "en") ? "You solved XXX out of ZZZ correctly." : "Sie haben XXX von ZZZ richtig gelöst."

function randomNumber(min, max) {
    return Math.floor(Math.random() * (max - min) + min);
}

function shuffle(a) {
    var j, x, i;
    for (i = a.length - 1; i > 0; i--) {
        j = Math.floor(Math.random() * (i + 1));
        x = a[i];
        a[i] = a[j];
        a[j] = x;
    }
    return a;
}

function createNumbers(num_cells) {
    var nums = new Array(num_cells);
    nums[0] = randomNumber(11, 89);
    nums[1] = target_sum - nums[0];
    console.log(nums);
    for (var r = 2; r < num_cells; r++) {
        nums[r] = randomNumber(10, 100);
        var p = 0;
        do {
            // console.log(nums[r] + " + " + nums[p] + " = " + (nums[r] + nums[p]));
            // This is the check to see if any other pairs add up to 100 too, or if we have duplicate numbers
            if (nums[r] + nums[p] == 100 || nums[r] == nums[p]) {
                console.log("Duplicate or 100 sum TRUE: reset:");
                // They do, so allocate another number, and reset so we go over the whole lot again
                nums[r] = randomNumber(10, 100);
                p = 0;
                console.log(nums[r]);
            }
            p++;
        }
        while (p < r)
    }
    shuffle(nums);
    console.log(nums);
    return nums;
}

createNumbers(9);

function createMatrix(row, col) {
    active_box1 = 0;
    active_box2 = 0;
    active_sum = 0;

    clickstream.push(new Clickstream())

    var numbers = createNumbers(row * col);

    var table = "<table width=\"" + (col * 80) + "\" height=\"" + (row * 80) + "\" style=\"vertical-align: middle; text-align: center;\" cellpadding=\"15\">";
    var h = 0;
    for (var i = 1; i <= row; i++) {
        table += "<tr>";
        for (var j = 1; j <= col; j++) {
            table += "<td style=\"width: 80px; height: 80px; text-align: center; vertical-align: middle;\"><div id=\"box_" + i + j + "\" style=\"width: 50px; height: 50px; display: flex; justify-content: center; align-items: center; font-size: 20px; color: #2C9C91; border: 2px solid #2C9C91; border-radius: 10px; cursor: pointer;\">" + numbers[h] + "</div></td>";
            h += 1;
        }
        table += "</tr>";
    }
    table += "</table>";
    return table;
}

function selectBox(i, j) {

    if (active_box1 == 0 || active_box2 == 0 || active_box1 == "" + i + "," + j || active_box2 == "" + i + "," + j) {

        if (document.getElementById('box_' + i + j).style.backgroundColor != "rgb(44, 156, 145)") {
            document.getElementById('box_' + i + j).style.backgroundColor = "#2C9C91";
            document.getElementById('box_' + i + j).style.color = "#FFFFFF";

            if (active_box1 == 0 && active_box2 == 0) {
                active_box1 = "" + i + "," + j;
            }
            else if (active_box1 == 0 && active_box2 != 0) {
                active_box1 = "" + i + "," + j;
            }
            else if (active_box1 != 0 && active_box2 == 0) {
                active_box2 = "" + i + "," + j;
            }
        }
        else {
            document.getElementById('box_' + i + j).style.backgroundColor = "#FFFFFF";
            document.getElementById('box_' + i + j).style.color = "#2C9C91";

            if (active_box1 == "" + i + "," + j) {
                active_box1 = 0;
            }
            else if (active_box2 == "" + i + "," + j) {
                active_box2 = 0;
            }
        }
    }


    if (!(active_box1 == 0 || active_box2 == 0)) {
        var number1 = parseInt(document.getElementById('box_' + (active_box1.split(",")).join("")).innerHTML);
        var number2 = parseInt(document.getElementById('box_' + (active_box2.split(",")).join("")).innerHTML);
        active_sum = number1 + number2;
    }
    else {
        active_sum = 0;
    }
    // Record the click
    clickstream[clickstream.length - 1].addClick(i, j, document.getElementById('box_' + i + j).innerHTML);

    console.log(active_sum);
}

function submitAnswer() {
    // Update the clickstream with the end of the game
    clickstream[clickstream.length - 1].end(active_sum);
    // console.log("Clickstream: \n" + JSON.stringify(clickstream) + "\n");
    document.getElementById("clickstream").value = JSON.stringify(clickstream);


    if(active_sum == target_sum) {
        num_correct = num_correct + 1;
    }

    num_total = num_total + 1;

    document.getElementById("history_box").innerHTML = parseXZ(solved_text, num_correct, num_total);

    if (js_vars.liveUpdate) {
        // We are in a round and want to send an update to the server
        liveSend({ 'correct': num_correct, 'total': num_total })
    }
    document.getElementById("matrix").innerHTML = "";
    document.getElementById("matrix").innerHTML = createMatrix(3, 3);
    updateOnClicks();
}

function parseXZ(text, x, z) {
    text = text.replace(new RegExp("\\bXXX\\b", "gi"), x);
    text = text.replace(new RegExp("\\bZZZ\\b", "gi"), z);
    return text;
}

function updateOnClicks() {
    document.getElementById('box_11').onclick = function () { selectBox(1, 1); }
    document.getElementById('box_12').onclick = function () { selectBox(1, 2); }
    document.getElementById('box_13').onclick = function () { selectBox(1, 3); }
    document.getElementById('box_21').onclick = function () { selectBox(2, 1); }
    document.getElementById('box_22').onclick = function () { selectBox(2, 2); }
    document.getElementById('box_23').onclick = function () { selectBox(2, 3); }
    document.getElementById('box_31').onclick = function () { selectBox(3, 1); }
    document.getElementById('box_32').onclick = function () { selectBox(3, 2); }
    document.getElementById('box_33').onclick = function () { selectBox(3, 3); }
}


document.getElementById('matrix').innerHTML = createMatrix(3, 3)

// Attached event listeners: onclick
updateOnClicks();

document.getElementById('submitAnswerButton').onclick = function () {
    if (active_sum != 0) {
        submitAnswer();
    }
}

// Initialise some values
document.getElementById("submitAnswerButton").value = submit_text;
document.getElementById("history_box").innerHTML = parseXZ(solved_text, num_correct, num_total);
