from multiprocessing.connection import wait
from time import time
from otree.api import (
    models,
    widgets,
    BaseConstants,
    BaseSubsession,
    BaseGroup,
    BasePlayer,
    Currency as c,
    currency_range,
)
import logging
import random as random
from django.utils.translation import gettext as _

from settings import DROPOUT_STATES_DROPOUT, DROPOUT_STATES_UNGROUPED, DROPOUT_STATES_PLAYING, DROPOUT_STATES_VICTIM

logger = logging.getLogger(__name__)


doc = """
Your app description
"""


class Constants(BaseConstants):
    name_in_url = 'gender_lang'
    players_per_group = None
    num_rounds = 6

    # Timeouts
    grouping_wait_time = 480
    matrix_timeout = 120
    trial_timeout = 60
    trial_instructions_timeout = 180
    start_timeout = 180
    stage_1_end_timeout = 180
    stage_2_instructions_timeout = 180
    stage_2_end_timeout = 180
    stage_3_instructions_timeout = 210
    stage_3_decision_timeout = 180
    stage_4_instructions_timeout = 180
    stage_5_instructions_timeout = 300
    stage_5_voting_timeout = 210
    
    piece_rate = 500
    competition_rate = 2000
    collective_rate = 125
    collective_group = 500
    group_leader_bonus = 1000
    s1_bonus = 1000
    s2_bonus = 1000

    # These are displayeed after the '-' on each page for a stage and can be
    # indexed through "round_num" (Due to the blank first entry)
    page_titles = ["", _("Trial"), _("Piece Rate"), _("Competition"), _("Choice"), _("Group payment"), _("Election")]


class Subsession(BaseSubsession):
    def group_by_arrival_time_method(self, waiting_players):
        #
        # DEBUG This is only needed when testing, so I can skip the consent app
        #   and this block of code balances M/F's over 4 players so there is
        #   always a group
        #
        for p in waiting_players:
            if 'gender' not in p.participant.vars:
                p.participant.vars['gender'] = 'Male' if p.participant.id % 2 == 0 else 'Female'
                logger.info(f"Debug: Assigned gender: {p.participant.vars['gender']} to {p.participant.id}")
        #
        # DEBUG END
        #

        # We need groups containing 2M and 2F (Just group the 'Other's with the males)
        m_players = [p for p in waiting_players if p.participant.vars['gender']
                     == 'Male' or p.participant.vars['gender'] == "Männlich"]
        f_players = [p for p in waiting_players if p.participant.vars['gender']
                     == 'Female' or p.participant.vars['gender'] == "Weiblich"]

        if len(m_players) >= 2 and len(f_players) >= 2:
            logger.info('Creating a group')
            m_players[0].participant.vars['gender_code'] = "P1"
            m_players[1].participant.vars['gender_code'] = "P2"
            f_players[0].participant.vars['gender_code'] = "P3"
            f_players[1].participant.vars['gender_code'] = "P4"

            # Also set the dropout states of the players
            m_players[0].participant.vars['dropout_state'] = DROPOUT_STATES_PLAYING
            m_players[1].participant.vars['dropout_state'] = DROPOUT_STATES_PLAYING
            f_players[0].participant.vars['dropout_state'] = DROPOUT_STATES_PLAYING
            f_players[1].participant.vars['dropout_state'] = DROPOUT_STATES_PLAYING

            return [m_players[0], m_players[1], f_players[0], f_players[1]]

        for p in waiting_players:
            if has_waited_too_long(p):
                p.participant.vars['dropout_state'] = DROPOUT_STATES_UNGROUPED
                return [p]
                
        fs = [(p.participant.label or p.participant.code) for p in f_players]
        ms = [(p.participant.label or p.participant.code) for p in m_players]
        logger.info(
            f'Not enough players to create a group: {len(waiting_players)} (M:{len(m_players)}, F:{len(f_players)})\n'
            f'Females: {fs}, Males: {ms}')

def has_waited_too_long(player):
    elapsed_time = time() - player.participant.vars['wait_page_arrival']
    logger.info(f"Grouping wait for player since: {player.participant.vars['wait_page_arrival']}, elapsed: {elapsed_time}")
    return elapsed_time > Constants.grouping_wait_time

class Group(BaseGroup):
    group_leader_id = models.IntegerField()

    def assign_group_leader(self):
        players = self.get_players()
        # Electees are the people that want to be elected
        electees = [p for p in players if p.stage5_election]

        # We need to record the voting, regardless of whether they stood or not
        group_leader_votes = [0, 0, 0, 0]
        for i, p in enumerate(players):
            # Players have dropped out on the instructions page, so have not entered data when page times out
            # ignore their lack of inpout and allow the others to get past the wait page so they can move on to
            # dropout victims.
            if p.s4_ordering is not None:
                str_votes = p.s4_ordering[1:len(p.s4_ordering)-1].split(',')
                # The votes are id_in_group, so start at 1
                votes = list(map(int, str_votes))
                logger.info(f"Player: {p.id_in_group}, votes: {votes}")
                group_leader_votes[votes[0]-1] += 3
                group_leader_votes[votes[1]-1] += 2
                group_leader_votes[votes[2]-1] += 1
            else:
                mark_dropouts(p, "Stage 5 Voting")

        logger.info(f"group votes: {group_leader_votes}")
        # Now store this on the player
        for i, p in enumerate(players):
            p.s5_votes = group_leader_votes[i]
            logger.info(f"Player: {p.id_in_group}, vote score: {p.s5_votes}")

        # DEBUG
        stuff = [p.id_in_group for p in players if p.stage5_election]
        logger.info(f"\n\nElectees: {electees}, {stuff}")
        # END

        if len(electees) == 0:
            # No one stood, assign someone at random
            group_leader = random.choice(players)
            logger.info(f"Randomly assigned group leader: {group_leader.id_in_group}")

        elif len(electees) == 1:
            # Perfect, this person is the group leader
            group_leader = electees[0]
            logger.info(f"Only one person stood. Group leader: {group_leader.id_in_group}")
        else:
            # More than one - need to process the votes

            # Only need the votes for the people that stood for election
            electee_votes = []
            for e in electees:
                electee_votes.append(group_leader_votes[e.id_in_group-1])
            top_vote = max(electee_votes)
            logger.info(
                f"Considering the following votes: {electee_votes}, top vote: {top_vote}, from: {group_leader_votes}")
            if len(electees) != len(electee_votes):
                raise Exception(
                    f'Wrong number of electees or votes for them. Electees: {electees}, votes: {electee_votes}')

            # Find the number of people with this vote, that stood for election
            candidates = []
            for index, count in enumerate(electee_votes):
                if count == top_vote:
                    candidates.append(electees[index])

            logger.info(f"Candidates: {candidates}")
            group_leader = random.choice(candidates)

        group_leader.s5_group_leader = True
        group_leader.payoff = Constants.group_leader_bonus
        total = group_leader.num_correct
        self.group_leader_id = group_leader.id_in_group
        logger.info(f"Leader of the gang: {group_leader}, {group_leader.id_in_group}\n\n")

        # Everyone gets paid for each solution the group leader gets correct
        for p in players:
            p.payoff += total * Constants.piece_rate
            p.participant.vars["s5_payoff"] = p.payoff

    def set_payoffs(self):
        players = self.get_players()

        # Round number 1 is just the trial, there are no payoffs for it

        # Stage 1 - piece work, everyone gets the same
        if self.round_number == 2:
            self.payForWork(players)

        # Stage 2 - competition, most productive player gets paid lots, others get nothing
        if self.round_number == 3:
            for p in players:
                p.payoff = 0
                p.participant.vars["s2_payoff"] = p.payoff

            winner = self.getHighestEarner(players)
            winner.payoff = winner.num_correct * Constants.competition_rate
            winner.participant.vars["s2_payoff"] = winner.payoff

        # Payoff from this round depends on the users choice - either piece work, or competition
        if self.round_number == 4:
            winner = self.getHighestEarner(players)

            for p in players:
                if p.stage3_mode == 1:
                    # Piece work
                    p.payoff = p.num_correct * Constants.piece_rate
                    p.participant.vars["s3_payoff"] = p.payoff
                else:
                    # Competitive - winner takes all
                    if winner.participant.id == p.participant.id:
                        p.payoff = winner.num_correct * Constants.competition_rate
                        p.participant.vars["s3_payoff"] = p.payoff
                    else:
                        p.payoff = 0
                        p.participant.vars["s3_payoff"] = p.payoff

        # Everyone gets paid the same, for the group effort
        if self.round_number == 5:
            total = sum([p.num_correct for p in players])
            for p in players:
                p.payoff = total * Constants.collective_rate
                p.participant.vars["s4_payoff"] = p.payoff
                p.participant.vars["s4_perf"] = p.num_correct

        # Only one person can be the group-leader
        if self.round_number == 6:
            gl = self.get_player_by_id(self.group_leader_id)

            # Everyone gets paid for each solution the gl made
            for p in players:
                p.payoff += gl.num_correct * Constants.piece_rate
                p.participant.vars["s5_payoff"] = p.payoff

            # Then the group leader get a bonus
            gl.payoff += Constants.group_leader_bonus

    def payForWork(self, players):
        for p in players:
            if p.num_correct is None:
                # This could signify a dropout case - the user has not attempted to guess
                # or it could mean they just couldn't figure it out!
                p.num_correct = 0
            p.payoff = p.num_correct * Constants.piece_rate
            p.participant.vars["s1_payoff"] = p.payoff

    def getHighestEarner(self, players):
        #DEBUG
        # for p in players:
        #     logger.info(f"Player: {p.id}:{p.id_in_group}, numCorect: {p.num_correct}, round: {p.round_number}")
        # DEBUG END
        #         
        max_score = max([p.num_correct for p in players])

        # It's highly likely that there will be duplicates
        high_rollers = []
        for p in players:
            if p.num_correct == max_score:
                high_rollers.append(p)

        winner = random.choice(high_rollers)
        logger.info(f"Selected winner: {winner}, num correct: {winner.num_correct}")
        return winner

    def apply_stage_2_bonus(self):
        players = self.get_players()
        #DEBUG
        for p in players:
            logger.info(f"P.id: {p.id_in_group}, numCorect: {p.num_correct}")
        # DEBUG END

        # Get the sorted list of scores
        scores = sorted([p.num_correct for p in players], reverse=True)
        # Now a sorted list of unique values (deffo going to have duplicates)
        logger.info(f'Scores: {scores}')
        for player in players:
            num_draws = 0
            perf_id = 1
            for index, score in enumerate(scores):
                if player.num_correct == score and player.stage2_perf == perf_id:
                    player.stage2_perf_bonus = True
                    player.participant.vars['stage2_perf'] = c(Constants.s2_bonus)
                    logger.info(f"  Paying Bonus: {player.id_in_group}, Crct: {player.num_correct}, PerfId: {perf_id}")
                    break       # No point processing the rest
                elif index < 3 and scores[index] != scores[index+1]:
                    perf_id += (1 + num_draws)
                    # num_draws = 0
                else:
                    num_draws += 1

                if player.stage2_perf < perf_id:
                    # The player has guessed above their station, don't bother processing the rest
                    break



class Player(BasePlayer):
    num_correct = models.IntegerField(initial=0)
    num_total = models.IntegerField(initial=0)
    stage1_perf = models.IntegerField(widget=widgets.RadioSelect, label="")
    stage2_perf = models.IntegerField(widget=widgets.RadioSelect, label="", choices=[
        [1, _("I think I was the best of the four participants in my group")],
        [2, _("I think I was the second-best of the four participants in my group")],
        [3, _("I think I was the second-worst of the four participants in my group")],
        [4, _("I think I was the worst of the four participants in my group")],
    ])
    stage2_perf_bonus = models.BooleanField(initial=False)
    stage3_mode = models.IntegerField(widget=widgets.RadioSelect, choices=[
        [1, _("Piece-rate")],
        [2, _("Competition")]
    ])
    stage5_election = models.BooleanField(choices=[
        [True, _("I want to stand for election")],
        [False, _("I don't want to stand for election")]
    ])
    # This is the ordering of the id_in_group of the player after the Stage 5 election - with the first item at the top of the table
    s4_ordering = models.StringField()
    # This is the same thing, but broken out into separate fields - not used in the applicaition, requested by data processor
    s4_rank1 = models.IntegerField()
    s4_rank2 = models.IntegerField()
    s4_rank3 = models.IntegerField()
    # This is the total numbe of votes this person got (3 points for 1st place, 2 for 2nd, etc)
    s5_votes = models.IntegerField()
    s5_group_leader = models.BooleanField()
    clickstream = models.LongStringField()

    def live_update(self, data):
        self.num_correct = data['correct']
        self.num_total = data['total']
        # logger.info(f"Got update from page: Player: {self.id},{self.id_in_group}. Data {data}, {self.num_correct}, {self.num_total}. round: {self.round_number}")

    def stage1_perf_choices(self):
        if self.session.config['genderise'] == 'he':
            return [
                [1, _("I think I was in the top 25% of participants")],
                [2, _("I think I was in the top 50% of participants")],
                [3, _("I think I was in the bottom 50% of participants")],
                [4, _("I think I was among the bottom 25% of participants")],
            ]
        elif self.session.config['genderise'] == 'she':
            return [
                [1, _("I think I was in the top 25% of participants ")],
                [2, _("I think I was in the top 50% of participants ")],
                [3, _("I think I was in the bottom 50% of participants ")],
                [4, _("I think I was among the bottom 25% of participants ")],
            ]
        else:
            return [
                [1, _("I think I was in the top 25% of participants  ")],
                [2, _("I think I was in the top 50% of participants  ")],
                [3, _("I think I was in the bottom 50% of participants  ")],
                [4, _("I think I was among the bottom 25% of participants  ")],
            ]

    def stage2_perf_choices(self):
        if self.session.config['genderise'] == 'he':
            return [
                [1, _("I think I was the best of the four participants in my group")],
                [2, _("I think I was the second-best of the four participants in my group")],
                [3, _("I think I was the second-worst of the four participants in my group")],
                [4, _("I think I was the worst of the four participants in my group")],
            ]
        elif self.session.config['genderise'] == 'she':
            return [
                [1, _("I think I was the best of the four participants in my group ")],
                [2, _("I think I was the second-best of the four participants in my group ")],
                [3, _("I think I was the second-worst of the four participants in my group ")],
                [4, _("I think I was the worst of the four participants in my group ")],
            ]
        else:
            return [
                [1, _("I think I was the best of the four participants in my group  ")],
                [2, _("I think I was the second-best of the four participants in my group  ")],
                [3, _("I think I was the second-worst of the four participants in my group  ")],
                [4, _("I think I was the worst of the four participants in my group  ")],
            ]

    def is_playing(self):
        return self.participant.vars['dropout_state'] == DROPOUT_STATES_PLAYING

def mark_dropouts(player: Player, pagename: str):
        # This player is the dropout, the others are victims
        player.participant.vars['dropout_state'] = DROPOUT_STATES_DROPOUT
        player.participant.vars['dropout_info'] = pagename

        others = player.get_others_in_group()
        for o in others:
            o.participant.vars['dropout_state'] = DROPOUT_STATES_VICTIM
            o.participant.vars['dropout_info'] = pagename

