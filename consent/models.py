from os import environ
from otree.api import (
    models, widgets, BaseConstants, BaseSubsession, BaseGroup, BasePlayer, Currency as c
)
import logging
from common.definitions import nationality
from django.utils.translation import gettext as _
from settings import LANGUAGE_CODE
from common import definitions


logger = logging.getLogger(__name__)
author = 'Scott Vincent'


class Constants(BaseConstants):
    name_in_url = 'consent'
    players_per_group = None
    num_rounds = 1
    consent_timeout = int(environ.get('CONSENT_TIMEOUT', 120))  # This is in seconds


class Subsession(BaseSubsession):
    pass


class Group(BaseGroup):
    pass


class Player(BasePlayer):
    consent = models.BooleanField(widget=widgets.CheckboxInput, default=False, initial=False)
    browser_type = models.StringField(blank=True)
    screen_size = models.StringField(blank=True)

    age = models.IntegerField(min=18, max=120, label=_("Age"))
    nationality = models.StringField(label=_("Nationality"))
    # This lanuage is the one spoken on a daily basis
    language = models.StringField(label=_("What language do you speak the most on a daily basis?"))
    # This works, so you can't selete the ---- option, and it lifts the 2 main langs to the
    # top of the menu, but it means editing the whole of the definitions and adding each lang
    # twice. So I've parked this, so see if it's really needed
    #     choices=[
    #     ["En", "English"],
    #     ["Ge", "German"],
    #     [None, "---------"],
    #     ["Af", "Afrikaans"],
    #     ["Al", "Albanian"]]
    # )

    # This language is their native language
    native_language = models.StringField(label=_("What is your Native Language?"))

    gender = models.StringField(
        label=_("Gender"),
        choices=[_("Male"), _("Female")]
    )

    def consent_error_message(self, value):
        if not value:
            return _('You must accept the consent form in order to proceed with the study!')

    def conversion_rate(self):
        rate = 1/self.session.config['real_world_currency_per_point']
        return c(rate).to_real_world_currency(self.session), int(rate)

    def language_choices(self):
        choices = definitions.language[LANGUAGE_CODE]
        return choices

    def native_language_choices(self):
        choices = definitions.language[LANGUAGE_CODE]
        return choices

    def nationality_choices(self):
        choices = nationality[LANGUAGE_CODE]
        return choices
