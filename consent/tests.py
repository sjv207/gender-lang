from otree.api import *
from .pages import *
import logging

logger = logging.getLogger(__name__)

class PlayerBot(Bot):

    def play_round(self):

        yield Consent, dict(consent=True)
        print(f"---- PartID: {self.player.participant.id}")
        if self.player.participant.id % 2 == 0:
            yield Welcome, dict(age=20, gender="Male", language="English", nationality = "British", native_language="English")
        else:
            yield Welcome, dict(age=20, gender="Female", language="English", nationality = "British", native_language="English")
        yield Introduction
