from otree.api import Currency as c, currency_range
from ._builtin import Page, WaitPage
from .models import Constants, Player
from django.utils.translation import gettext as _
from settings import DROPOUT_STATES_DROPOUT, DROPOUT_STATES_VICTIM, LANGUAGE_CODE
import logging


logger = logging.getLogger(__name__)


def getPageTitle(round_number: int):
    return _("Stage ") + str(round_number - 1) + " - " + Constants.page_titles[round_number]


def mark_dropouts(player: Player, timeout_happened: bool, pagename: str):
    if timeout_happened:
        # This player is the dropout, the others are victims
        player.participant.vars['dropout_state'] = DROPOUT_STATES_DROPOUT
        player.participant.vars['dropout_info'] = pagename

        others = player.get_others_in_group()
        for o in others:
            o.participant.vars['dropout_state'] = DROPOUT_STATES_VICTIM
            o.participant.vars['dropout_info'] = pagename


class GroupingWaitPage(WaitPage):
    # body_text = _("Please wait for other participants to finish reading the instructions.")
    group_by_arrival_time = True

    def is_displayed(self):
        return self.round_number == 1

    def vars_for_template(self):
        if self.session.config['genderise'] == 'he':
            body_text = _("Please wait for other participants to finish reading the instructions" \
                ", and then you will be grouped. This can take up to 8 min so please be patient.")
        elif self.session.config['genderise'] == 'she':
            body_text = _("Please wait for other participants to finish reading the instructions" \
                ", and then you will be grouped. This can take up to 8 min so please be patient. ")
        else:
            body_text = _("Please wait for other participants to finish reading the instructions" \
                ", and then you will be grouped. This can take up to 8 min so please be patient.  ")
        return dict(body_text=body_text)


class TrialInstructions(Page):
    timeout_seconds = Constants.trial_instructions_timeout

    def is_displayed(self):
        return self.round_number == 1 and self.player.is_playing()

    def vars_for_template(self):
        return dict(language=LANGUAGE_CODE)

    def before_next_page(self):
        mark_dropouts(self.player, self.timeout_happened, _("Trial round instructions"))


class Trial(Page):
    timeout_seconds = Constants.trial_timeout
    live_method = 'live_update'
    form_model = 'player'
    form_fields = ['clickstream']

    def vars_for_template(self):
        return dict(page_title=getPageTitle(self.round_number), matrix_title=_("Trial round"))

    def js_vars(self):
        return dict(num_total=0, num_correct=0, liveUpdate=True, language=LANGUAGE_CODE)

    def is_displayed(self):
        return self.round_number == 1 and self.player.is_playing()


class Start(Page):
    timeout_seconds = Constants.start_timeout

    def is_displayed(self):
        return self.round_number == 2 and self.player.is_playing()

    def vars_for_template(self):
        return dict(page_title=getPageTitle(self.round_number))

    def before_next_page(self):
        mark_dropouts(self.player, self.timeout_happened, getPageTitle(self.round_number))


class Stage2Instructions(Page):
    timeout_seconds = Constants.stage_2_instructions_timeout

    def is_displayed(self):
        return self.round_number == 3 and self.player.is_playing()

    def vars_for_template(self):
        return dict(page_title=getPageTitle(self.round_number))

    def before_next_page(self):
        mark_dropouts(self.player, self.timeout_happened, getPageTitle(self.round_number))


class Stage3Instructions(Page):
    timeout_seconds = Constants.stage_3_instructions_timeout

    def is_displayed(self):
        return self.round_number == 4 and self.player.is_playing()

    def vars_for_template(self):
        return dict(page_title=getPageTitle(self.round_number))


class Stage3Decision(Page):
    timeout_seconds = Constants.stage_3_decision_timeout
    form_model = 'player'
    form_fields = ['stage3_mode']

    def is_displayed(self):
        return self.round_number == 4 and self.player.is_playing()

    def vars_for_template(self):
        return dict(page_title=getPageTitle(self.round_number))

    def before_next_page(self):
        mark_dropouts(self.player, self.timeout_happened, getPageTitle(self.round_number))


class Stage4Instructions(Page):
    timeout_seconds = Constants.stage_4_instructions_timeout

    def is_displayed(self):
        return self.round_number == 5 and self.player.is_playing()

    def vars_for_template(self):
        return dict(page_title=getPageTitle(self.round_number))

    def before_next_page(self):
        mark_dropouts(self.player, self.timeout_happened, getPageTitle(self.round_number))


class Stage5Instructionsm(Page):
    timeout_seconds = Constants.stage_5_instructions_timeout
    form_model = 'player'
    form_fields = ['stage5_election']

    def is_displayed(self):
        return self.round_number == 6 and self.session.config['genderise'] == 'he' and self.player.is_playing()

    def vars_for_template(self):
        return dict(page_title=getPageTitle(self.round_number))

    def before_next_page(self):
        mark_dropouts(self.player, self.timeout_happened, getPageTitle(self.round_number))


class Stage5Instructionsf(Page):
    timeout_seconds = Constants.stage_5_instructions_timeout
    form_model = 'player'
    form_fields = ['stage5_election']

    def is_displayed(self):
        return self.round_number == 6 and self.session.config['genderise'] == 'she' and self.player.is_playing()

    def vars_for_template(self):
        return dict(page_title=getPageTitle(self.round_number))

    def before_next_page(self):
        mark_dropouts(self.player, self.timeout_happened, getPageTitle(self.round_number))


class Stage5Instructionst(Page):
    timeout_seconds = Constants.stage_5_instructions_timeout
    form_model = 'player'
    form_fields = ['stage5_election']

    def is_displayed(self):
        return self.round_number == 6 and self.session.config['genderise'] == 'they' and self.player.is_playing()

    def vars_for_template(self):
        return dict(page_title=getPageTitle(self.round_number))

    def before_next_page(self):
        mark_dropouts(self.player, self.timeout_happened, getPageTitle(self.round_number))


def voting_sorter(elem):
    # Sort first on score, then on participant code where there are draws
    return (elem[2], elem[5])


class Stage5Voting(Page):
    timeout_seconds = Constants.stage_5_voting_timeout
    form_model = 'player'
    form_fields = ['s4_ordering', 's4_rank1', 's4_rank2', 's4_rank3']

    def vars_for_template(self):
        codes = []
        perfs = []
        genders = []
        colours = ["#f7d1cd", "#dff0f5", "#e8dff2"]
        ids = []
        p_id = []

        others = self.player.get_others_in_group()
        for o in others:
            codes.append(o.participant.vars["gender_code"])
            genders.append(o.participant.vars['gender'])
            perfs.append(o.participant.vars["s4_perf"])
            ids.append(o.id_in_group)
            p_id.append(o.participant.code)

        zipped_data = zip(codes, genders, perfs, colours, ids, p_id)
        # Convert to list so we can sort it
        zipped_data = list(zipped_data)
        sorted_zip = sorted(zipped_data, key=voting_sorter, reverse=True)
        return dict(stage4_data=sorted_zip, page_title=getPageTitle(self.round_number))

    def is_displayed(self):
        return self.round_number == 6 and self.player.is_playing()

    def js_vars(self):
        return dict(language=LANGUAGE_CODE)

    def before_next_page(self):
        mark_dropouts(self.player, self.timeout_happened, getPageTitle(self.round_number))


class VotingWaitPage(WaitPage):
    # body_text = _("Please wait for the other participants to finish voting for the group-leader")
    after_all_players_arrive = 'assign_group_leader'

    def is_displayed(self):
        return self.round_number == 6 and self.player.is_playing()

    def vars_for_template(self):
        if self.session.config['genderise'] == 'he':
            body_text = _("Please wait for the other participants to finish voting for the group-leader.")
        elif self.session.config['genderise'] == 'she':
            body_text = _("Please wait for the other participants to finish voting for the group-leader. ")
        else:
            body_text = _("Please wait for the other participants to finish voting for the group-leader.  ")
        return dict(body_text=body_text)


class InstructionsWaitPage(WaitPage):

    def is_displayed(self):
        return self.round_number != 1 and self.round_number != 6 and self.player.is_playing()

    def vars_for_template(self):
        # # Round 6 needs a different wait page
        # if self.round_number == 6:
        #     return False

        if self.round_number == 3:
            body_text = _("Please wait to be grouped.")
        elif self.session.config['genderise'] == 'he':
            body_text = _("Please wait for the other participants.")
        elif self.session.config['genderise'] == 'she':
            body_text = _("Please wait for the other participants. ")
        else:
            body_text = _("Please wait for the other participants.  ")

        return dict(body_text=body_text)


class PreStage1Instructions(WaitPage):
    body_text = _("The instructions for Stage 1 will appear shortly...")

    def is_displayed(self):
        return self.round_number == 1 and self.player.is_playing()


# class PreMatrixWait(WaitPage):
#     body_text = _("Matrix task about to start...")

#     def is_displayed(self):
#         return self.round_number != 1 and self.player.is_playing()


class Matrix(Page):
    live_method = 'live_update'
    timeout_seconds = Constants.matrix_timeout
    form_model = 'player'
    form_fields = ['clickstream']

    def vars_for_template(self):
        # Round 1 is the trial round, so the first true round is actually round 2, but we want
        # to show it as round one, hence -1
        stage = str(self.round_number-1)
        return dict(page_title=getPageTitle(self.round_number), matrix_title=_("Quiz ") + stage)

    def js_vars(self):
        num_total = self.player.num_total if self.player.num_total is not None else 0
        num_correct = self.player.num_correct if self.player.num_correct is not None else 0
        # DEBUG
        # logger.info(f"js_vars: {dict(num_total=num_total, num_correct=num_correct, liveUpdate=True, language=LANGUAGE_CODE)}")
        # ENBD
        return dict(num_total=num_total, num_correct=num_correct, liveUpdate=True, language=LANGUAGE_CODE)

    def is_displayed(self):
        if not self.player.is_playing():
            return False

        # Stage 5 (round 6) is a special case - only those who stood for election get to play
        if self.round_number == 1:
            return False
        elif self.round_number == 6:
            if self.player.stage5_election or self.player.s5_group_leader:
                # It is possible that no one wanted to be elected, so someone was shotgunned into power
                return True
            else:
                return False
        else:
            return True


class MatrixWaitPage(WaitPage):
    after_all_players_arrive = 'set_payoffs'

    def vars_for_template(self):
        # In round 6, if the player is sitting out, show a slightly different message
        if self.round_number == 6 and not (self.player.stage5_election or self.player.s5_group_leader):
            if self.session.config['genderise'] == 'he':
                body_text = _("Please wait while the participants that opted to stand for election complete the task.")
            elif self.session.config['genderise'] == 'she':
                body_text = _("Please wait while the participants that opted to stand for election complete the task. ")
            else:
                body_text = _("Please wait while the participants that opted to stand for election complete the task.  ")
        else:
            if self.session.config['genderise'] == 'he':
                body_text = _("Please wait for the other participants.")
            elif self.session.config['genderise'] == 'she':
                body_text = _("Please wait for the other participants. ")
            else:
                body_text = _("Please wait for the other participants.  ")
        return dict(body_text=body_text)

    def is_displayed(self):
        return self.player.is_playing()


class Stage1End(Page):
    timeout_seconds = Constants.stage_1_end_timeout
    form_model = 'player'
    form_fields = ['stage1_perf']

    def is_displayed(self):
        return self.round_number == 2 and self.player.is_playing()

    def before_next_page(self):
        mark_dropouts(self.player, self.timeout_happened, getPageTitle(self.round_number))


class Stage2End(Page):
    timeout_seconds = Constants.stage_2_end_timeout
    form_model = 'player'
    form_fields = ['stage2_perf']

    def is_displayed(self):
        return self.round_number == 3 and self.player.is_playing()

    def before_next_page(self):
        mark_dropouts(self.player, self.timeout_happened, getPageTitle(self.round_number))


class Stage2EndWait(WaitPage):
    after_all_players_arrive = 'apply_stage_2_bonus'

    def is_displayed(self):
        return self.round_number == 3 and self.player.is_playing()

    def vars_for_template(self):
        if self.session.config['genderise'] == 'he':
            body_text = _("Please wait for the other participants.")
        elif self.session.config['genderise'] == 'she':
            body_text = _("Please wait for the other participants. ")
        else:
            body_text = _("Please wait for the other participants.  ")

        return dict(body_text=body_text)


class Results(Page):
    def is_displayed(self):
        return self.round_number == Constants.num_rounds and self.session.config['show_debug_results'] and self.player.is_playing()

    def vars_for_template(self):
        all_rounds = self.player.in_all_rounds()
        data = []
        for i, p in enumerate(all_rounds):
            stage_data = []
            stage_data.append(p.num_correct)
            stage_data.append(p.num_total)
            if i == 3:
                if 'stage2_perf' in p.participant.vars:
                    stage_data.append(p.participant.vars['stage2_perf'])
                else:
                    stage_data.append(False)
            else:
                stage_data.append("")
            stage_data.append(p.stage3_mode if p.stage3_mode is not None else "")
            stage_data.append(p.s4_ordering if p.s4_ordering is not None else "")
            stage_data.append(p.stage5_election if p.stage5_election is not None else "")
            stage_data.append(p.s5_group_leader if p.s5_group_leader is not None else "")
            if i == 0:
                stage_data.append("NA")
            else:
                stage_data.append(p.participant.vars['s' + str(i) + "_payoff"])

            data.append(stage_data)
        return dict(data=data)


page_sequence = [
    GroupingWaitPage, 
    TrialInstructions,
    Trial, Start,
    Stage2Instructions, Stage3Instructions, Stage3Decision,
    Stage4Instructions,
    Stage5Instructionsm, Stage5Instructionsf, Stage5Instructionst, Stage5Voting, VotingWaitPage,
    InstructionsWaitPage, PreStage1Instructions, Matrix, MatrixWaitPage,
    Stage1End, Stage2End, Stage2EndWait,
    Results]
