### To rebuild the translations
From the root directory run these commands:

1. django-admin makemessages -l de
2. django-admin makemessages -l en
3. django-admin compilemessages

**NOTE: You will need to do this on the server, as the .mo files don't get committed to git**