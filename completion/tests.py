from otree.api import *
from .pages import *
import logging


class PlayerBot(Bot):
    def play_round(self):
        yield Involvement, dict(
            involvement_1=1,
            involvement_2=2,
            involvement_3=3,
            involvement_4=4,
            involvement_5=5,
            involvement_6=4,
            involvement_7=3,
        )
        yield Questionnaire1, dict(
            country_of_residence="United Kingdom",
            manager_gender="Male",
            role_model_gender=1,
            risk=3
        )
        yield Questionnaire2, dict(
            education="A-levels",
            vote="Other",
            uni_student=False,
            party_support=3,
        )
        yield Att, dict(
            attention_1=1,
            attention_2=2,
            attention_3=3,
            attention_4=4,
            attention_5=5,
        )
        yield Debrief, dict(
            debrief_1="aaa",
            debrief_2="aaa",
            debrief_3="aaa",
            debrief_4="aaa",
        )

        # yield Results
