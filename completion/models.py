from typing import DefaultDict
from otree.api import (
    models,
    widgets,
    BaseConstants,
    BaseSubsession,
    BaseGroup,
    BasePlayer,
    Currency as c,
)
from django.utils.translation import gettext as _
from common import definitions
from settings import DROPOUT_STATES_PLAYING, LANGUAGE_CODE


author = 'Scott Vincent'


def make_field(label):
    return models.IntegerField(label=label, choices=Constants.likert5, widget=widgets.RadioSelect)


def make_field_10(label):
    return models.IntegerField(label=label, choices=Constants.likert10, widget=widgets.RadioSelect)


class Constants(BaseConstants):
    name_in_url = 'completion'
    players_per_group = None
    num_rounds = 1
    likert5 = [[i + 1, ''] for i in range(5)]
    likert10 = [[i + 1, ''] for i in range(10)]
    understanding_gender_bonus = 6000   # Bonus in ECU for Understanding page


class Subsession(BaseSubsession):
    pass


class Group(BaseGroup):
    pass


class Player(BasePlayer):
    paid_stage = models.IntegerField()
    involvement_1 = make_field('')  # This message appears on the page for translation reasons
    involvement_2 = make_field('')  # This message appears on the page for translation reasons
    involvement_3 = make_field(_('I thought it was inappropriate to participate in the competition in Stage 3.'))
    involvement_4 = make_field('')  # This message appears on the page for translation reasons
    involvement_5 = make_field(_('I felt like an equal member of the group.'))
    involvement_6 = make_field(_('I felt like I belonged to my group.'))
    involvement_7 = make_field(_('I felt included in my group.'))
    country_of_residence = models.StringField()
    manager_gender = models.StringField(
        choices=[_("Male"), _("Female"), _("Other"), _("NA")]
    )
    role_model_gender = make_field('')  # This message appears on the page for translation reasons
    risk = make_field_10('')
    education = models.StringField()
    uni_student = models.BooleanField(widget=widgets.RadioSelectHorizontal, choices=[
        [True, _("Yes")],
        [False, _("No")]
    ])
    attention_1 = make_field(_('I made my decisions in this study carefully'))
    attention_2 = make_field(_('I made the decisions in this study randomly'))
    attention_3 = make_field(_('I understood what my decisions meant for my payment'))
    attention_4 = make_field(_('Please check the option on the far left'))
    attention_5 = make_field(_('Please check the option on the far right'))

    debrief_1 = models.LongStringField(label=_('What do you think this study was about?'), blank=True)
    debrief_2 = models.LongStringField(label=_('Did you find one or more parts of the study strange? '
                                       'Or did you wonder about one or more parts?'), blank=True)
    debrief_3 = models.LongStringField(label=_('Do you think some of the parts of the study are related? '
                                       'If so, what was the connection?'), blank=True)
    debrief_4 = models.LongStringField(label=_('Has anything you did in one part influenced your behaviour '
                                       'or performance in another part? If so, how exactly was your behaviour '
                                               '(your performance) affected?'), blank=True)

    # These fields are for the pilot page and only populated in the English version
    problems = models.LongStringField(label="Did you encounter any problems in the study?", blank=True)
    typos = models.LongStringField(label="Did you see any typos?", blank=True)
    comments = models.LongStringField(label="Anything else you want to tell us?", blank=True)

    vote = models.StringField()
    party_support = make_field(_('To what extent do you support this party?'))

    # This is only used where participants get paid via email, not required for UoE or Prolific participants
    email = models.StringField()
    email_confirmation = models.StringField()

    # This is the final status of the player, as all paid players end up at the thank you page need this
    # to differentiate the dropout victims, ungrouped and completers.
    status = models.StringField(initial=DROPOUT_STATES_PLAYING)

    understanding_gender = models.IntegerField(choices=[
        [1, "Während der Studie werden alle Teilnehmer aufgefordert, Entscheidungen zu treffen. Sowohl Ihre eigenen "
            "Entscheidungen, als auch die der anderen Teilnehmer bestimmen Ihre Auszahlung gemäß den auf den nächsten Seiten "
            "erläuterten Regeln. Jeder Teilnehmer trifft seine Entscheidungen anonym und alle Antworten bleiben vertraulich. "
            "Alle Teilnehmer sehen die gleichen Anleitungen, die auf den folgenden Seiten für alle Teilnehmer ausführlich beschrieben werden."],
        [2, "Während der Studie werden alle Teilnehmer/Teilnehmerinnen aufgefordert, Entscheidungen zu treffen. Sowohl Ihre "
            "eigenen Entscheidungen, als auch die der anderen Teilnehmer/Teilnehmerinnen bestimmen Ihre Auszahlung gemäß den "
            "auf den nächsten Seiten erläuterten Regeln. Jeder Teilnehmer/Jede Teilnehmerin trifft seine/ihre Entscheidungen anonym, "
            "und alle Antworten bleiben vertraulich. Alle Teilnehmer/Teilnehmerinnen sehen die gleichen Anleitungen, die auf den "
            "folgenden Seiten für alle Teilnehmer/Teilnehmerinnen ausführlich beschrieben werden."],
        [3, "Während der Studie werden alle Teilnehmenden aufgefordert, Entscheidungen zu treffen. Sowohl Ihre eigenen "
            "Entscheidungen, als auch die der anderen Teilnehmenden bestimmen Ihre Auszahlung gemäß den auf den nächsten Seiten "
            "erläuterten Regeln. Teilnehmende treffen ihre Entscheidungen anonym und alle Antworten bleiben vertraulich. Alle "
            "Teilnehmenden sehen die gleichen Anleitungen, die auf den folgenden Seiten für alle Teilnehmenden ausführlich beschrieben werden."]
    ], widget=widgets.RadioSelect,
        label="Welche Instruktionen hatten Sie? Bitte wählen Sie eine Variante aus:")
    understanding_gender_bonus = models.IntegerField(initial=0.0)

    def country_of_residence_choices(self):
        choices = definitions.country[LANGUAGE_CODE]
        return choices

    def language_choices(self):
        choices = definitions.language[LANGUAGE_CODE]
        return choices

    def vote_choices(self):
        # This depends on the country of residence. However, not all countries are covered in the
        # data. In that case we look up the one based on the language this study is run in
        if self.country_of_residence in definitions.political_parties[LANGUAGE_CODE]:
            choices = definitions.political_parties[LANGUAGE_CODE][self.country_of_residence]
        else:
            country = "United Kingdom" if LANGUAGE_CODE == 'en' else "Deutschland"
            choices = definitions.political_parties[LANGUAGE_CODE][country]
        return choices

    def education_choices(self):
        choices = definitions.education[LANGUAGE_CODE]
        return choices

    def conversion_rate(self):
        rate = 1/self.session.config['real_world_currency_per_point']
        return c(rate).to_real_world_currency(self.session), int(rate)

    def is_playing(self):
        # Only need to check existence so that this app can be tested standalone
        if 'dropout_state' in self.participant.vars:
            return self.participant.vars['dropout_state'] == DROPOUT_STATES_PLAYING
        else:
            return True
