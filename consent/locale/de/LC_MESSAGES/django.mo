��          �   %   �      P  L   Q    �  �   �  �     �   W  �   .  �   		  �   �	  P   �
  �    �  �  �  =     �     �     �  ;   �          *     /  %   ;  [   a     �     �  5   �  D   '  �   l  I   Y  |  �         #  �   @  �   =     )  �   J  L   8  �  �  �  F  �         �!     �!  
   �!  L   �!     3"  	   ?"     I"  5   ^"  \   �"     �"     #  0   /#  V   `#                                                                                      
                         	              

<p>We first ask that you please fill out the following questionnaire:</p>
 
<div class="panel panel-danger">
    <div class="panel-heading"><strong>Attention!</strong></div>
    <div class="panel-body">
        <p><strong>You have <b>%(consent_timeout_min)s</b> minutes to read and accept this
                Consent Form.</strong></p>
    </div>
</div>
<div class="row">
    <div class="col-sm-12">
        <div class="box box1 shadow1">
            <h3>Informed Consent Information</h3>
            <p>This is a study on economic decision making.</p>

            <p>By proceeding with the experiment, you provide consent that you understand the information provided and
                agree to participate in this study. You also understand that your participation is voluntary and you may
                withdraw at any time.</p>

             
<p>During the study, all participants will be asked to make decisions. Both your own decisions and those of the other
    participants determine your payout according to the rules explained on the next pages.  </p>
 
<p>During the study, all participants will be asked to make decisions. Both your own decisions and those of the other
    participants determine your payout according to the rules explained on the next pages. </p>
 
<p>During the study, all participants will be asked to make decisions. Both your own decisions and those of the other
    participants determine your payout according to the rules explained on the next pages.</p>
 
<p>Each participant makes his decisions anonymously, and all answers remain confidential. All participants see the
    same instructions, which are described in detail for all participants on the following pages.</p>
 
<p>Each participant makes his/her decisions anonymously, and all answers remain confidential. All participants see the
    same instructions, which are described in detail for all participants on the following pages.</p>
 
<p>Each participant makes their decisions anonymously, and all answers remain confidential. All participants see the
    same instructions, which are described in detail for all participants on the following pages.</p>
 
<p>The currency in the experiment is the Experimental Currency Unit (ECU).</p>
 
<p>The experiment consists of <strong>five stages</strong>. One of the five stages is selected at random for payout of
    any bonus earned at the rate of <strong>%(ecus)s ECU = %(conversion_rate)s </strong>. The total earnings of each
    participant from the experiment are the sum of his payments for the randomly selected stage (bonus) plus a
    participation fee of %(participation_fee)s. </p>
 
<p>The experiment consists of <strong>five stages</strong>. One of the five stages is selected at random for payout of
    any bonus earned at the rate of <strong>%(ecus)s ECU = %(conversion_rate)s</strong>. The total earnings of each
    participant from the experiment are the sum of his/her payments for the randomly selected
    stage (bonus) plus a participation fee of %(participation_fee)s. </p>
 
<p>The experiment consists of <strong>five stages</strong>. One of the five stages is selected at random for payout of
    any bonus earned at the rate of <strong>%(ecus)s ECU = %(conversion_rate)s</strong>. The total earnings of each
    participant from the experiment are the sum of their payments for the randomly selected
    stage (bonus) plus a participation fee of %(participation_fee)s.</p>
 Age Female Gender I acknowledge that I have read the rules and privacy policy Introduction Male Nationality Please, accept this consent agreement The instructions for the respective stages are displayed one after the other on the screen. Welcome to our Study! What is your Native Language? What language do you speak the most on a daily basis? You must accept the consent form in order to proceed with the study! Project-Id-Version: genderlang
Report-Msgid-Bugs-To: 
PO-Revision-Date: 
Last-Translator: 
Language-Team: 
Language: de
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 3.0.1
 

<p>Wir bitten Sie zunächst den folgenden Fragebogen auszufüllen:</p>
 
<div class="panel Panel-Gefahr">
    <div class="panel-heading"><strong>Achtung!</strong></div>
    <div class="panel-body">
        <p><strong>Sie haben <b>%(consent_timeout_min)s</b> Minuten Zeit,  um die Einverständniserklärung 
                zu lesen und zu akzeptieren.</strong></p>
    </div>
</div>
<div class="row">
    <div class="col-sm-12">
        <div class="box box1 shadow1">
            <h3>Informationen zur Einverständniserklärung</h3>
            <p>Dies ist eine Studie zur wirtschaftlichen Entscheidungsfindung (s.g. Experiment).</p>

            <p>Indem Sie mit dem Experiment fortfahren, stimmen Sie zu, dass Sie die bereitgestellten Informationen verstehen und
                einverstanden sind, an dieser Studie teilzunehmen. Sie verstehen auch, dass Ihre Teilnahme freiwillig ist und Sie können diese
                jederzeit widerrufen.</p>

             
<p>Während der Studie werden alle Teilnehmenden aufgefordert, Entscheidungen zu treffen. Sowohl Ihre eigenen Entscheidungen, als auch die der anderen
    Teilnehmenden bestimmen Ihre Auszahlung gemäß den auf den nächsten Seiten erläuterten Regeln.</p>
 
<p>Während der Studie werden alle Teilnehmer/Teilnehmerinnen aufgefordert, Entscheidungen zu treffen. Sowohl Ihre eigenen Entscheidungen, als auch die der anderen
    Teilnehmer/Teilnehmerinnen bestimmen Ihre Auszahlung gemäß den auf den nächsten Seiten erläuterten Regeln.</p>
 
<p>Während der Studie werden alle Teilnehmer aufgefordert, Entscheidungen zu treffen. Sowohl Ihre eigenen Entscheidungen, als auch die der anderen
    Teilnehmer bestimmen Ihre Auszahlung gemäß den auf den nächsten Seiten erläuterten Regeln.</p>
 
<p>Jeder Teilnehmer trifft seine Entscheidungen anonym und alle Antworten bleiben vertraulich. Alle Teilnehmer sehen die
    gleichen Anleitungen, die auf den folgenden Seiten für alle Teilnehmer ausführlich beschrieben werden.</p>
 
Jeder Teilnehmer/Jede Teilnehmerin trifft seine/ihre Entscheidungen anonym, und alle Antworten bleiben vertraulich. Alle Teilnehmer/Teilnehmerinnen sehen die
    gleichen Anleitungen, die auf den folgenden Seiten für alle Teilnehmer/Teilnehmerinnen ausführlich beschrieben werden.</p>
 
<p>Teilnehmende treffen ihre Entscheidungen anonym und alle Antworten bleiben vertraulich. Alle Teilnehmenden sehen die
    gleichen Anleitungen, die auf den folgenden Seiten für alle Teilnehmenden ausführlich beschrieben werden.</p>
 
<p>Die Währung im Experiment lautet Experimental Currency Unit (ECU).</p>
 
<p>Das Experiment besteht aus <strong>fünf Stufen</strong>. Eine der fünf Stufen wird zufällig für die Bonusauszahlung ausgewählt
    und jeder verdiente Bonus wird mit dem Wechselkurs <strong>%(ecus)s ECU = %(conversion_rate)s</strong> ausgezahlt. Der Gesamtverdienst 
    jedes Teilnehmers aus dem Experiment ist die Summe seiner Zahlungen für die zufällig ausgewählte Stufe plus 
    die Teilnahmegebühr von %(participation_fee)s.</p>
 
<p>Das Experiment besteht aus <strong>fünf Stufen</strong>. Eine der fünf Stufen wird zufällig für die Bonusauszahlung ausgewählt
    und jeder verdiente Bonus wird mit dem Wechselkurs <strong>%(ecus)s ECU = %(conversion_rate)s</strong> ausgezahlt. Der Gesamtverdienst 
    jedes Teilnehmers/jeder Teilnehmerin aus dem Experiment ist die Summe seiner/ihrer Zahlungen für die zufällig ausgewählte Stufe plus 
    die Teilnahmegebühr von %(participation_fee)s.</p>
 
<p>Das Experiment besteht aus <strong>fünf Stufen</strong>. Eine der fünf Stufen wird zufällig für die Bonusauszahlung ausgewählt
    und jeder verdiente Bonus wird mit dem Wechselkurs <strong>%(ecus)s ECU = %(conversion_rate)s</strong> ausgezahlt. Der Gesamtverdienst 
    aus dem Experiment ist die Summe der Zahlungen für die zufällig ausgewählte Stufe plus 
    die Teilnahmegebühr von %(participation_fee)s.</p>
 Alter Weiblich Geschlecht Ich bestätige, dass ich die Regeln und Datenschutzbestimmungen gelesen habe Einführung Männlich Staatsangehörigkeit Bitte akzeptieren Sie diese Einverständniserklärung Die Anleitungen für die jeweiligen Stufen werden nacheinander auf dem Bildschirm angezeigt. Willkommen bei unserer Studie! Welche ist Ihre Muttersprache? Welche Sprache sprechen Sie täglich am meisten? Sie müssen die Einverständniserklärung akzeptieren, um mit der Studie fortzufahren! 