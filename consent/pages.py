from os import stat
from time import time

from settings import DROPOUT_STATES_NON_CONSENT, DROPOUT_STATES_PLAYING
from .models import *
from .pages import *

from otree.api import *
import logging
import math


logger = logging.getLogger(__name__)


class Consent(Page):
    timeout_seconds = Constants.consent_timeout
    form_model = 'player'
    form_fields = ['consent']
    timeout_submission = {'consent': False}

    def vars_for_template(self):
        return {'consent_timeout_min': math.ceil(Constants.consent_timeout / 60)}

    def before_next_page(self):
        if self.timeout_happened:
            self.player.consent = False
            self.player.participant.vars['consent_dropout'] = True

    def app_after_this_page(self, upcoming_apps):
        if self.player.consent == False:
            logger.info(f"Player did not consent, moving them on. Label: {self.player.participant.label}")
            self.player.participant.vars['dropout_state'] = DROPOUT_STATES_NON_CONSENT
            return upcoming_apps[-1]


class Welcome(Page):
    form_model = 'player'
    form_fields = ['browser_type', 'screen_size', 'age', 'gender', 'nationality', 'language', 'native_language']

    def before_next_page(self):
        # Write the gender and language into participant vars for use in other apps
        self.participant.vars['gender'] = self.player.gender
        self.participant.vars['language'] = self.player.language


class Introduction(Page):
    def before_next_page(self):
        # Need to log when they started waiting to be grouped
        self.participant.vars['wait_page_arrival'] = time()
        logger.info(f"{self.participant.label or self.participant.id}: Start time: f{self.participant.vars['wait_page_arrival']}")


page_sequence = [Consent, Welcome, Introduction]
